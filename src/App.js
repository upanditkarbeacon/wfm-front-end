import React from "react";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import history from "./history";
import getStore from "./store/store";
import "bootstrap/dist/css/bootstrap.css";
import "./styles/LineIcons.min.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import "./App.css";

import Dashboard from "./components/Dashboard/Dashboard";
import Login from "./components/Login/Login";
import ForgotPassword from "./components/User/ForgotPassword/ForgotPassword";
import ResetPassword from "./components/User/ForgotPassword/ResetPassword";
import  ExportWorkflow  from "./components/ExportWorkflow/ExportWorkflow";

const AuthenticatedRoute = () => {
  if (!localStorage.getItem("loginUser")) {
    return <Redirect to="/login" />;
  }

  return (
    <Switch>
      <Route path="/Dashboard" component={Dashboard} />

    </Switch>
  );
};

export default () => (
  <div>
    <Provider store={getStore()}>
      <Router history={history}>
        <Switch>
        <Route path="/" component={Login} exact/>
          <Route path="/login" component={Login} />
          <Route path="/forgotPassword" component={ForgotPassword} />
          <Route path="/resetPassword/:authToken" component={ResetPassword} />
          <Route path="/ExportWorkflow/:workflowId" component={ExportWorkflow} />


          <AuthenticatedRoute />
        </Switch>
        <ToastContainer autoClose={2000} /> {/* For toaster message */}
      </Router>
    </Provider>
  </div>
);
/* function App() {
  return (
    <div>
    <Provider store={getStore()}>
      <Router history={history}>
        <Switch>
          <Route path="/Dashboard" component={Dashboard} />
          <Route path="/" component={Login} />
       </Switch>
      </Router>
      <ToastContainer autoClose={2000} /> 
    </Provider>
  </div>
  );
} */
