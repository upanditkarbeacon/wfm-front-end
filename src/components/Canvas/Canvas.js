import * as go from "gojs";
import { ReactDiagram } from "gojs-react";
import * as React from "react";
import nodeData from "../../constants/nodeData.json";
import "gojs/extensions/Figures";
import { getNodes } from "../../utility/getNodes";
import "../../App.css";
import { contextMenuList } from "../../utility/getDiagramContextMenu";
import {
  setReference,
  linkProblemConverter
} from "../../utility/validateWorkflow";

export class Canvas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nodeDataValues: nodeData.nodeData,
      graphLinks: [],
      workflowDraft: [],
    };
    this.diagramRef = React.createRef();
    this.initDiagram = this.initDiagram.bind(this);
    this.getValueForNodes = this.getValueForNodes.bind(this);
  }

  async contextMenu(diagram, $) {
    var data = [];
    await this.props.getValues().then((res) => {
      if (res.payload.status === 200) {
        data = res.payload.data.node_data;
      }
      contextMenuList(diagram, $, data);
    });
  }
  componentDidMount() {
    if (!this.diagramRef.current) return;
    const diagram = this.diagramRef.current.getDiagram();
    if (diagram instanceof go.Diagram) {
      diagram.addDiagramListener("ChangedSelection", this.props.onDiagramEvent);
    }
  }

  async getValueForNodes($, diagram) {
    var data = [];
    await this.props.getValues().then((res) => {
      if (res.payload.status === 200) {
        data = res.payload.data.node_data;
      }
    });
    getNodes(diagram, $, data);
    setReference(diagram, $);
    $(
      go.Palette,
      "myPaletteDiv", // must name or refer to the DIV HTML element
      {
        "animationManager.initialAnimationStyle": go.AnimationManager.None,
        nodeTemplateMap: diagram.nodeTemplateMap,
        model: new go.GraphLinksModel(data),
      }
    );
    return data;
  }
  componentDidUpdate(prevProps) {
    const diagram = this.diagramRef.current.getDiagram();
    if (prevProps.loadImportDiagram !== this.props.loadImportDiagram) {
      diagram.model = go.Model.fromJson({
        class: "GraphLinksModel",
        linkKeyProperty: "key",
        nodeDataArray: this.props.loadImportDiagram[0].nodeDataArray,
        linkDataArray: this.props.loadImportDiagram[0].linkDataArray,
      });
    }

    if (prevProps.nodeDataArray !== this.props.nodeDataArray) {
      this.setState({
        workflowDraft: [
          {
            nodeDataArray: this.props.nodeDataArray,
            linkDataArray: this.props.linkDataArray,
          },
        ],
      });
    }

    if (prevProps.linkDataArray !== this.props.linkDataArray) {
      this.setState({
        workflowDraft: [
          {
            nodeDataArray: this.props.nodeDataArray,
            linkDataArray: this.props.linkDataArray,
          },
        ],
      });
    }
    if (prevProps.initialNodeValues !== this.props.initialNodeValues) {
    }
  }

  /**
   * Get the diagram reference and remove listeners that were added during mounting.
   */
  componentWillUnmount() {
    if (!this.diagramRef.current) return;
    const diagram = this.diagramRef.current.getDiagram();
    if (diagram instanceof go.Diagram) {
      diagram.removeDiagramListener(
        "ChangedSelection",
        this.props.onDiagramEvent
      );
    }
  }


  initDiagram() {
    const $ = go.GraphObject.make;
    const diagram = $(go.Diagram, {
      // set your license key here before creating the diagram: go.Diagram.licenseKey = "...";
      hoverDelay: 10,
     // "linkReshapingTool": new SnapLinkReshapingTool(),
      "undoManager.isEnabled": true, // must be set to allow for model change listening
      // 'undoManager.maxHistoryLength': 0,  // uncomment disable undo/redo functionality
      /*   "clickCreatingTool.archetypeNodeData": {
        text: "new node",
        color: "lightblue",
      }, */
      LinkDrawn: linkProblemConverter,

      //  layout: $(go.ForceDirectedLayout),
      model: $(go.GraphLinksModel, {
        linkLabelKeysProperty: "labelKeys",
        linkKeyProperty: "key", // IMPORTANT! must be defined for merges and data sync when using GraphLinksModel
        // positive keys for nodes
        makeUniqueKeyFunction(m, data) {
          let k = data.key || 1;
          while (m.findNodeDataForKey(k)) k++;
          data.key = k;
          return k;
        },
        // negative keys for links
        makeUniqueLinkKeyFunction: (m, data) => {
          let k = data.key || -1;
          while (m.findLinkDataForKey(k)) k--;
          data.key = k;
          return k;
        },
      }),
    });
   diagram.scrollMode= diagram.InfiniteScroll;
   //diagram.scrollMode=diagram.DocumentScroll;
    // diagram.contextMenu = this.contextMenu(diagram, $);
    diagram.toolManager.linkingTool.archetypeLabelNodeData = diagram.addDiagramListener(
      "ExternalObjectsDropped",
      function (e) {
        e.subject.each(function (node) {
          var model = e.diagram.model;
         /*  if (node.data.category === "Case Type") {
            model.setDataProperty(node.data, "Case Type", "Case Type");
          }
          if (node.data.category === "Outcome") {
            model.setDataProperty(node.data, "Outcome", "Outcome");
          }
          if (node.data.category === "Disposition Outcome") {
            model.setDataProperty(
              node.data,
              "Disposition Outcome",
              "Disposition Outcome"
            );
          }
          if (node.data.category === "Task") {
            model.setDataProperty(node.data, "Task", "Task");
          }
          if (node.data.category === "step") {
            model.setDataProperty(node.data, "Step", "Step");
          }
          if (node.data.category === "Note") {
            model.setDataProperty(node.data, "Note", "Note");
          } */
          model.setDataProperty(node.data, "text", "");
        });
      }
    );

    /*  diagram.linkTemplate =
    $("Link",
      { relinkableFrom: true, relinkableTo: true, toShortLength: 2 },
      $("Shape", { stroke: "#2E68CC", strokeWidth: 2 }),
      $("Shape", { fill: "#2E68CC", stroke: null, toArrow: "Standard" })
    );
    diagram.linkTemplateMap.add("linkToLink",
    $("Link",
      { relinkableFrom: true, relinkableTo: true },
      $("Shape", { stroke: "#2D9945", strokeWidth: 2 })
    )); */

    diagram.linkTemplate = $(
      go.Link,
      { toShortLength: 4 },
      { 
            routing: go.Link.AvoidsNodes,
            curve: go.Link.JumpOver,
            corner: 5, toShortLength: 4,
            relinkableFrom: true,
            relinkableTo: true,
            reshapable: true,
            resegmentable: true,
            // mouse-overs subtly highlight links:
          //  mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          //  mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; },
            selectionAdorned: false},
      $(
        go.Shape,
        new go.Binding("stroke", "isHighlighted", function (h) {
          return h ? "red" : "grey";
        }).ofObject(),
        new go.Binding("strokeWidth", "isHighlighted", function (h) {
          return h ? 2 : 2;
        }).ofObject()
      ),
      $(
        go.Shape,
        { toArrow: "Standard", strokeWidth: 0 },
        new go.Binding("fill", "isHighlighted", function (h) {
          return h ? "red" : "grey";
        }).ofObject()
      )
    );
    this.getValueForNodes($, diagram);
    return diagram;
  }

  render() {
    return (
      <ReactDiagram
        ref={this.diagramRef}
        divClassName="myDiagramDiv"
        className="myDiagramDiv"
        initDiagram={this.initDiagram}
        nodeDataArray={this.props.nodeDataArray}
        linkDataArray={this.props.linkDataArray}
        modelData={this.props.modelData}
        onModelChange={this.props.onModelChange}
        skipsDiagramUpdate={this.props.skipsDiagramUpdate}
      />
    );
  }
}
