import * as go from "gojs";
import { produce } from "immer";
import * as React from "react";
import { Canvas } from "../Canvas/Canvas";
import SelectionInspector from "../DataInspector/SelectionInspector";
import { Container, Row, Col } from "react-bootstrap";
import Header from "../Header/Header";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getAllNodes } from "../../store/actions/sample";
import { Footer } from "../Footer/Footer";
import "../../App.css";
import Loader from "../Loader/Loader"
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nodeDataArray: [],
      content: {},
      linkDataArray: [],
      showLoader:false,
      modeDataWorkflow: [],
      validateWorkflow: false,
      modelData: {
        canRelink: true,
      },
      selectedData: null,
      skipsDiagramUpdate: false,
    };
    // init maps
    this.mapNodeKeyIdx = new Map();
    this.mapLinkKeyIdx = new Map();
    this.refreshNodeIndex(this.state.nodeDataArray);
    this.refreshLinkIndex(this.state.linkDataArray);
    // bind handler methods
    this.handleDiagramEvent = this.handleDiagramEvent.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleRelinkChange = this.handleRelinkChange.bind(this);
  }

  /**
   * Update map of node keys to their index in the array.
   */
  refreshNodeIndex(nodeArr) {
    this.mapNodeKeyIdx.clear();
    nodeArr.forEach((n, idx) => {
      this.mapNodeKeyIdx.set(n.key, idx);
    });
  }

  /**
   * Update map of link keys to their index in the array.
   */
  refreshLinkIndex(linkArr) {
    this.mapLinkKeyIdx.clear();
    linkArr.forEach((l, idx) => {
      this.mapLinkKeyIdx.set(l.key, idx);
    });
  }

  importDiagram(content) {
    if (content && content[0]) {
      this.setState({
        nodeDataArray: content[0].nodeDataArray,
        linkDataArray: content[0].linkDataArray,
        content: content,
      });
      this.refreshNodeIndex(this.state.nodeDataArray);
      this.refreshLinkIndex(this.state.linkDataArray);
      return content;
    }
  }
  showLoader=()=>{
    this.setState({
      showLoader :true
    })
  }
  hideLoader=()=>{
    this.setState({
      showLoader :false
    })
  }

  validateWorkflow(showValidationFor) {
    this.setState({
      validateWorkflow: true,
      showValidationFor: showValidationFor,
    });
  }

  /**
   * Handle any relevant DiagramEvents, in this case just selection changes.
   * On ChangedSelection, find the corresponding data and set the selectedData state.
   * @param e a GoJS DiagramEvent
   */
  handleDiagramEvent(e) {
    const name = e.name;
    switch (name) {
      case "ChangedSelection": {
        const sel = e.subject.first();
        this.setState(
          produce((draft) => {
            if (sel) {
              if (sel instanceof go.Node) {
                const idx = this.mapNodeKeyIdx.get(sel.key);
                if (idx !== undefined && idx >= 0) {
                  const nd = draft.nodeDataArray[idx];
                  draft.selectedData = nd;
                }
              } else if (sel instanceof go.Link) {
                const idx = this.mapLinkKeyIdx.get(sel.key);
                if (idx !== undefined && idx >= 0) {
                  const ld = draft.linkDataArray[idx];
                  draft.selectedData = ld;
                }
              }
            } else {
              //  draft.selectedData = null;
            }
          })
        );
        break;
      }
      case "": {
        console.log("diagram modified");
        break;
      }
      default:
        break;
    }
  }

  /**
   * Handle GoJS model changes, which output an object of data changes via Model.toIncrementalData.
   * This method iterates over those changes and updates state to keep in sync with the GoJS model.
   * @param obj a JSON-formatted string
   */
  handleModelChange(obj) {
    const insertedNodeKeys = obj.insertedNodeKeys;
    const modifiedNodeData = obj.modifiedNodeData;
    const removedNodeKeys = obj.removedNodeKeys;
    const insertedLinkKeys = obj.insertedLinkKeys;
    const modifiedLinkData = obj.modifiedLinkData;
    const removedLinkKeys = obj.removedLinkKeys;
    const modifiedModelData = obj.modelData;

    this.setState({
      modeDataWorkflow: obj,
    });
    // maintain maps of modified data so insertions don't need slow lookups
    const modifiedNodeMap = new Map();
    const modifiedLinkMap = new Map();
    this.setState(
      produce((draft) => {
        let narr = draft.nodeDataArray;
        if (modifiedNodeData) {
          modifiedNodeData.forEach((nd) => {
            modifiedNodeMap.set(nd.key, nd);
            const idx = this.mapNodeKeyIdx.get(nd.key);
            if (idx !== undefined && idx >= 0) {
              narr[idx] = nd;
              if (draft.selectedData && draft.selectedData.key === nd.key) {
                draft.selectedData = nd;
              }
            }
          });
        }
        if (insertedNodeKeys) {
          insertedNodeKeys.forEach((key) => {
            const nd = modifiedNodeMap.get(key);
            const idx = this.mapNodeKeyIdx.get(key);
            if (nd && idx === undefined) {
              // nodes won't be added if they already exist
              this.mapNodeKeyIdx.set(nd.key, narr.length);
              narr.push(nd);
            }
          });
        }
        if (removedNodeKeys) {
          narr = narr.filter((nd) => {
            if (removedNodeKeys.includes(nd.key)) {
              return false;
            }
            return true;
          });
          draft.nodeDataArray = narr;
          this.refreshNodeIndex(narr);
        }

        let larr = draft.linkDataArray;
        if (modifiedLinkData) {
          modifiedLinkData.forEach((ld) => {
            modifiedLinkMap.set(ld.key, ld);
            const idx = this.mapLinkKeyIdx.get(ld.key);
            if (idx !== undefined && idx >= 0) {
              larr[idx] = ld;
              if (draft.selectedData && draft.selectedData.key === ld.key) {
                draft.selectedData = ld;
              }
            }
          });
        }
        if (insertedLinkKeys) {
          insertedLinkKeys.forEach((key) => {
            const ld = modifiedLinkMap.get(key);
            const idx = this.mapLinkKeyIdx.get(key);
            if (ld && idx === undefined) {
              // links won't be added if they already exist
              this.mapLinkKeyIdx.set(ld.key, larr.length);
              larr.push(ld);
            }
          });
        }
        if (removedLinkKeys) {
          larr = larr.filter((ld) => {
            if (removedLinkKeys.includes(ld.key)) {
              return false;
            }
            return true;
          });
          draft.linkDataArray = larr;
          this.refreshLinkIndex(larr);
        }
        // handle model data changes, for now just replacing with the supplied object
        if (modifiedModelData) {
          draft.modelData = modifiedModelData;
        }
        draft.skipsDiagramUpdate = true; // the GoJS model already knows about these updates
      })
    );
  }

  /**
   * Handle inspector changes, and on input field blurs, update node/link data state.
   * @param path the path to the property being modified
   * @param value the new value of that property
   * @param isBlur whether the input event was a blur, indicating the edit is complete
   */
  handleInputChange(path, value, isBlur) {
    this.setState(
      produce((draft) => {
       
        const data = draft.selectedData; // only reached if selectedData isn't null
        data[path] = value;
        console.log("value",value,path,data)
        if(path === "Task" || path==="Case Type" || path === "Outcome"){
          data["Description"]=value;
        }
        if (isBlur) {
          const key = data.key;
          // if (key) {
          // negative keys are links
          const idx = this.mapLinkKeyIdx.get(key);
          if (idx !== undefined && idx >= 0) {
            draft.linkDataArray[idx] = data;
            draft.skipsDiagramUpdate = false;
          }
          // } else {
          const idx1 = this.mapNodeKeyIdx.get(key);
          if (idx1 !== undefined && idx1 >= 0) {
            draft.nodeDataArray[idx1] = data;
            draft.skipsDiagramUpdate = false;
          }
          // }
        }
      })
    );
  }

  remove(node) {
    var array = [...this.state.nodeDataArray];
    if (array.key === "Initia Note") {
      array.splice(0, 1);
      this.setState({ nodeDataArray: array });
    }
  }
  /**
   * Handle changes to the checkbox on whether to allow relinking.
   * @param e a change event from the checkbox
   */
  handleRelinkChange(e) {
    // const target = e.target;
    //const value = target.checked;
    this.setState({
      modelData: { canRelink: true },
      skipsDiagramUpdate: false,
    });
  }

  render() {
    const selectedData = this.state.selectedData;
    let inspector;
    if (selectedData !== null) {
      inspector = (
        <SelectionInspector
          selectedData={this.state.selectedData}
          nodeDataArray={this.state.nodeDataArray}
          onInputChange={this.handleInputChange}
          validateWorkflow={this.state.validateWorkflow}
          showValidationFor={this.state.showValidationFor}
        />
      );
    }

    return (
      <div>
        <Container fluid>
          <Row>
            <Header
              importDiagram={(e) => this.importDiagram(e)}
              validateWorkflow={(e) => this.validateWorkflow(e)}
              nodeDataArray={this.state.nodeDataArray}
              linkDataArray={this.state.linkDataArray}
              showLoader= {this.showLoader}
              hideLoader={this.hideLoader}
            ></Header>
          </Row>
          <Row>
            <Col>
              <div className="mainDiv">
                <div id="myPaletteDiv" className="myPallateDiv"></div>
       
                <Canvas
                  nodeDataArray={this.state.nodeDataArray}
                  loadImportDiagram={this.state.content}
                  getValues={(e) => this.props.getAllNodes()}
                  initialNodeValues={this.props.nodeDataArray}
                  linkDataArray={this.state.linkDataArray}
                  modelData={this.state.modelData}
                  skipsDiagramUpdate={this.state.skipsDiagramUpdate}
                  onDiagramEvent={this.handleDiagramEvent}
                  selectedData={this.state.selectedData}
                  onModelChange={this.handleModelChange}
                  remove={(narr) => this.remove(narr)}
                  modeDataWorkflow={this.state.modeDataWorkflow}
                />
              </div>
      
              {inspector}
            </Col>
          </Row>
        </Container>
        {this.state.showLoader ? 
        <Loader/>:<div></div>}
        <Footer></Footer>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getAllNodes: getAllNodes,
    },
    dispatch
  );
}
function mapStateToProps({ workflowReducer }) {
  return {
    nodeDataArray: workflowReducer.nodeDataArray,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Dashboard)
);
