import React from "react";
import { Input, Row, Col } from "reactstrap";
import { FormGroup } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getCaseTypes } from "../../store/actions/sample";
import {
  getPredefinedValues,
  saveToPredefinedValueList,
} from "../../store/actions/workflow.action";
import { Button, Popover, PopoverHeader, PopoverBody } from "reactstrap";
import { toast } from "react-toastify";
/*  import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faLeaf } from "@fortawesome/free-solid-svg-icons"; */
import "./DataInspector.css";

class InspectorRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      popoverOpen: false,
      popoverOpenCaseType:false,
      popoverOpenCaseCategory:false,
      popoverOpenOutcome:false,
      popoverOpenTriggerOutcome:false,
      popoverOpenReviewType:false,
      popoverOpenActivityType:false,
      popoverOpenActivity:false,
      predefinedValueActivity: "",
      predefinedValueActivityType: "",
      predefinedValueCaseType: "",
      predefinedValueCaseCategory: "",
      predefinedValueOutcome: "",
      predefinedValueTriggerOutcome: "",
      predefinedValueReviewType: "",
      reviewTypeId: "",
      activityId: "",
      Task:false,
      activityTypeId: "",
      outcomeId: "",
      triggerOutcomeId: "",
      caseTypeId: "",
      caseCategoryId: "",
      configurationKey: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.checkPredefinedValue = this.checkPredefinedValue.bind(this);
    this.popOverData = this.popOverData.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  checkPredefinedValueNode(node,value){
    if(value){
      if(node === "Case Type"){
        this.popOverData("CaseTypeNode")
        const exists = JSON.parse(this.state.predefinedValueCaseType).some(
          (v) => v.value === value
        );
        if (!exists) {
          this.setState({
            popoverOpenCaseType: true,
          });
          this.setState({
            configurationId: this.state.caseTypeId,
            configurationValue:value,
            configurationKey: "CaseType",
          });
        }
      }
      }
      if(node === "Outcome"){
        this.popOverData("OutcomeNode")
        const exists = JSON.parse(this.state.predefinedValueOutcome).some(
          (v) => v.value === value
        );
        if (!exists) {
          this.setState({
            popoverOpenOutcome: true,
          });
          this.setState({
            configurationId: this.state.outcomeId,
            configurationValue:value,
            configurationKey: "Outcome",
          });
        }
      }
  }
  checkPredefinedValue(e) {
    this.handleInputChange(e)
    if (e.target.value) {
      if (e.target.name === "ReviewTypeNode") {
        const exists = JSON.parse(this.state.predefinedValueReviewType).some(
          (v) => v.value === e.target.value
        );
        if (!exists) {
 
          this.setState({
            configurationId: this.state.reviewTypeId,
            configurationValue: e.target.value,
            configurationKey: "ReviewType",
            popoverOpenReviewType:true
          });
        }
      }
      if (e.target.name === "OutcomeNode") {
        const exists = JSON.parse(this.state.predefinedValueOutcome).some(
          (v) => v.value === e.target.value
        );
        if (!exists) {

          this.setState({
            configurationId: this.state.outcomeId,
            configurationValue: e.target.value,
            configurationKey: "Outcome",
            popoverOpenOutcome:true
          });
        }
      }
      if (e.target.name === "CaseTypeNode") {
        const exists = JSON.parse(this.state.predefinedValueCaseType).some(
          (v) => v.value === e.target.value
        );
        if (!exists) {

          this.setState({
            configurationId: this.state.caseTypeId,
            configurationValue: e.target.value,
            configurationKey: "CaseType",
            popoverOpenCaseType:true
          });
        }
      }
      if (e.target.name === "CaseCategoryNode") {
        const exists = JSON.parse(this.state.predefinedValueCaseCategory).some(
          (v) => v.value === e.target.value
        );
        if (!exists) {

          this.setState({
            configurationId: this.state.caseCategoryId,
            configurationValue: e.target.value,
            configurationKey: "CaseCategory",
            popoverOpenCaseCategory:true
          });
        }
      }
      if (e.target.name === "ActivityNode") {
        const exists = JSON.parse(this.state.predefinedValueActivity).some(
          (v) => v.value === e.target.value
        );
        if (!exists) {

          this.setState({
            configurationId: this.state.activityId,
            configurationValue: e.target.value,
            configurationKey: "Activity",
            popoverOpenActivity :true
          });
        }
      }
      if (e.target.name === "ActivityTypeNode") {
        const exists = JSON.parse(this.state.predefinedValueActivityType).some(
          (v) => v.value === e.target.value
        );
        if (!exists) {

          this.setState({
            configurationId: this.state.activityTypeId,
            configurationValue: e.target.value,
            configurationKey: "ActivityType",
            popoverOpenActivityType:true
          });
        }
      }
      if (e.target.name === "TriggerOutcomeNode") {
        const exists = JSON.parse(
          this.state.predefinedValueTriggerOutcome
        ).some((v) => v.value === e.target.value);
        if (!exists) {
          this.setState({
            configurationId: this.state.triggerOutcomeId,
            configurationValue: e.target.value,
            configurationKey: "TriggerOutcome",
            popoverOpenTriggerOutcome:true
          });
        }
      }
    }
  }
  handleInputChange(e) {
    
    this.props.onInputChange(
      this.props.id,
      e.target.value,
      e.type === "blur",
      this.props.type
    );
  }

  componentDidMount() {
    if (this.props.id === "Activity" || this.props.id === "OActivity") {
      this.props.getPredefinedValues("Activity");
    }
    if (this.props.id === "Activity Type") {
      this.props.getPredefinedValues("ActivityType");
    }
    if (this.props.id === "Outcome") {
      this.props.getPredefinedValues("Outcome");
    }
    if (this.props.id === "Case Type") {
      this.props.getPredefinedValues("CaseType");
    }
    if (this.props.id === "Review Type") {
      this.props.getPredefinedValues("ReviewType");
    }
    if (this.props.id === "Trigger Outcome") {
      this.getValuesForTriggerOutcome()
    //  this.props.getPredefinedValues("TriggerOutcome");
    }
    if (this.props.id === "Case Category") {
      this.props.getPredefinedValues("CaseCategory");
    }
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen,
    });
  }

  closePopOver = (e) => {
   var id=e.target.value
     if(id === "CaseTypeNode" ){
       this.setState({
        popoverOpenCaseType:false
       })
     }
     if(id === "CaseCategoryNode" ){
      this.setState({
        popoverOpenCaseCategory:false
      })
    }
    if(id === "ActivityNode" ){
      this.setState({
        popoverOpenActivity:false
      })
    }
    if(id === "ActivityTypeNode" ){
      this.setState({
        popoverOpenActivityType:false
      })
    }
    if(id === "OutcomeNode" ){
      this.setState({
        popoverOpenOutcome:false
      })
    }
    if(id === "ReviewTypeNode" ){
      this.setState({
        popoverOpenReviewType:false
      })
    }
    if(id === "TriggerOutcomeNode" ){
      this.setState({
        popoverOpenTriggerOutcome:false
      })
    } 
     this.setState({
      popoverOpen: false,
    }); 
  };

  saveToPredefinedValList = (key) => {
    if (this.state.configurationValue) {
      var newValueToList = { value: this.state.configurationValue };
      this.props.saveToPredefinedValueList(
        this.state.configurationId,
        newValueToList
      );
    }
  };

  popOverData(id) {
    return (

      <div>
        <Popover
          placement="right"
          isOpen={id === "CaseTypeNode" ?this.state.popoverOpenCaseType:
                  id==="CaseCategoryNode"?this.state.popoverOpenCaseCategory:
                  id === "ActivityNode"  ? this.state.popoverOpenActivity:
                  id === "ActivityTypeNode" ? this.state.popoverOpenActivityType:
                  id === "OutcomeNode" ? this.state.popoverOpenOutcome:
                  id === "ReviewTypeNode" ? this.state.popoverOpenReviewType:
                  id === "TriggerOutcomeNode" ? this.state.popoverOpenTriggerOutcome:false}
          target={id}
        >
          <PopoverHeader>Add this value to predefined list?</PopoverHeader>
          <PopoverBody>
            <Button
              className="popover-button"
              color="info"
              size="sm"
              outline 
              onClick={this.saveToPredefinedValList}
            >
              Yes
            </Button>

            <Button
              size="sm"
              className="popover-button"
              color="info"
              outline 
              value={id}
              onClick={(e)=>this.closePopOver(e,"value")}
            >
              No
            </Button>
          </PopoverBody>
        </Popover>
      </div>
    );
  }
  getValuesForTriggerOutcome(){
    var triggerOutcome = [];
       if(this.props.nodeDataArray){
        for(var i in this.props.nodeDataArray){
          if(this.props.nodeDataArray[i].category === "Outcome"){
            if(!triggerOutcome.includes(this.props.nodeDataArray[i].Outcome)){
              triggerOutcome.push(this.props.nodeDataArray[i].Outcome)
            }
             this.setState({
              predefinedValueTriggerOutcome: triggerOutcome,
            }); 
      }
    
    }
    }
  }

  componentDidUpdate(prevProps) {

    if(prevProps.selectedData !== this.props.selectedData){
      if(this.props.selectedData){
        if(this.props.id === "Case Type"){
          this.setState({
            popoverOpenCaseType:false,
          })
            this.checkPredefinedValueNode("Case Type",this.props.selectedData)
        }
        if(this.props.id === "Outcome"){
          this.setState({
            popoverOpenOutcome :false
          })
          this.checkPredefinedValueNode("Outcome",this.props.selectedData)
      }
      }
    }
 /*    if(prevProps.caseTypeValue!==this.props.caseTypeValue){
      if(this.props.caseTypeValue){
        this.checkPredefinedValueNode("Case Type",this.props.caseTypeValue)
      }
    } */
    if (prevProps.getPredefinedValuesData !== this.props.getPredefinedValuesData) {
      if(this.props.getPredefinedValuesData && this.props.getPredefinedValuesData.data&& this.props.getPredefinedValuesData.data.length>0 ){
      if (this.props.getPredefinedValuesData.status === 200 || this.props.getPredefinedValuesData.status === 201) {
        if (this.props.getPredefinedValuesData.data[0].configurationKey === "Activity")
         {
          this.setState({
            predefinedValueActivity: this.props.getPredefinedValuesData.data[0].configurationValues,
            activityId: this.props.getPredefinedValuesData.data[0].configuredValueId,
          });
        }
        if (this.props.getPredefinedValuesData.data[0].configurationKey ==="CaseType") {
          this.setState({
            predefinedValueCaseType: this.props.getPredefinedValuesData.data[0].configurationValues,
            caseTypeId: this.props.getPredefinedValuesData.data[0].configuredValueId,
          });
        }
        if (
          this.props.getPredefinedValuesData.data[0].configurationKey ==="CaseCategory" ) {
          this.setState({
            predefinedValueCaseCategory: this.props.getPredefinedValuesData.data[0].configurationValues,
            caseCategoryId: this.props.getPredefinedValuesData.data[0].configuredValueId,
          });
        }
        if (this.props.getPredefinedValuesData.data[0].configurationKey === "Outcome")
         {
          this.setState({
            predefinedValueOutcome: this.props.getPredefinedValuesData.data[0]
              .configurationValues,
            outcomeId: this.props.getPredefinedValuesData.data[0]
              .configuredValueId,
          });
        }
        if (
          this.props.getPredefinedValuesData.data[0].configurationKey ===
          "ReviewType"
        ) {
          this.setState({
            predefinedValueReviewType: this.props.getPredefinedValuesData
              .data[0].configurationValues,
            reviewTypeId: this.props.getPredefinedValuesData.data[0]
              .configuredValueId,
          });
        }
       /*  if (
          this.props.getPredefinedValuesData.data[0].configurationKey ===
          "TriggerOutcome"
        ) */ /* {
          this.setState({
            predefinedValueTriggerOutcome: this.props.getPredefinedValuesData
              .data[0].configurationValues,
            triggerOutcomeId: this.props.getPredefinedValuesData.data[0]
              .configuredValueId,
          });
        } */
        if (
          this.props.getPredefinedValuesData.data[0].configurationKey ==="ActivityType") {
          this.setState({
            predefinedValueActivityType: this.props.getPredefinedValuesData
              .data[0].configurationValues,
            activityTypeId: this.props.getPredefinedValuesData.data[0]
              .configuredValueId,
          });
        }
      }
    }
  }
    if (
      prevProps.saveToPredefinedListData !== this.props.saveToPredefinedListData
    ) {
      if (this.props.saveToPredefinedListData.status === 200 ||this.props.saveToPredefinedListData.status === 201) {
        this.setState({
          popoverOpenCaseType: false,
          popoverOpenCaseCategory: false,
          popoverOpenOutcome: false,
          popoverOpenTriggerOutcome: false,
          popoverOpenReviewType: false,
          popoverOpenActivity: false,
          popoverOpenActivityType: false,
        });
      }
      if (this.state.configurationKey) {
        this.props.getPredefinedValues(this.state.configurationKey);
        toast.success(
          this.props.saveToPredefinedListData.data,
          {
            position: toast.POSITION.BOTTOM_RIGHT,
          }
        );
      }
    }
  }

  getDataList(key, val,maxLength) {
    var activityOptionData = [];
    var activityTypeOptionData = [];
    var outcomeOptionData = [];
    var triggerOutcomeOptionData = [];
    var reviewTypeOptionData = [];
    var caseCategoryOptionData = [];
    var caseTypeOptionData = [];
    if (this.state.predefinedValueActivityType) 
         {
           var valueParseActivityType = JSON.parse(this.state.predefinedValueActivityType);
           var objActivityType = Object.keys(valueParseActivityType);
      for (var itrActivityType = 0;itrActivityType < objActivityType.length;itrActivityType++) {
        if (valueParseActivityType) {
          activityTypeOptionData.push(
            <option
              key={itrActivityType}
              value={valueParseActivityType[itrActivityType].value}
            />
          );
        }
      }
    }
    if (this.state.predefinedValueTriggerOutcome) {
     /*  var valueParseTriggerOutcome = JSON.parse(
        this.state.predefinedValueTriggerOutcome
      ); */
    //  var objTriggerOutcome = Object.keys(valueParseTriggerOutcome);
      for (
        var itrTriggerOutcome = 0;
        itrTriggerOutcome < this.state.predefinedValueTriggerOutcome.length;
        itrTriggerOutcome++
      ) {
          triggerOutcomeOptionData.push(
            <option
              key={itrTriggerOutcome}
              value={this.state.predefinedValueTriggerOutcome[itrTriggerOutcome]}
            />
          );
        
      }
    }
    if (this.state.predefinedValueCaseType) {
      var valueParseCaseType = JSON.parse(this.state.predefinedValueCaseType);
      var objCaseType = Object.keys(valueParseCaseType);
      for (
        var itrCaseType = 0;
        itrCaseType < objCaseType.length;
        itrCaseType++
      ) {
        if (valueParseCaseType) {
          caseTypeOptionData.push(
            <option
              key={itrCaseType}
              value={valueParseCaseType[itrCaseType].value}
            />
          );
        }
      }
    }
    if (this.state.predefinedValueCaseCategory) {
      var valueParseCaseCategory = JSON.parse(
        this.state.predefinedValueCaseCategory
      );
      var objCaseCategory = Object.keys(valueParseCaseCategory);
      for (
        var itrCaseCat = 0;
        itrCaseCat < objCaseCategory.length;
        itrCaseCat++
      ) {
        if (valueParseCaseCategory) {
          caseCategoryOptionData.push(
            <option
              key={itrCaseCat}
              value={valueParseCaseCategory[itrCaseCat].value}
            />
          );
        }
      }
    }
    if (this.state.predefinedValueOutcome) {
      var valueParseOutcome = JSON.parse(this.state.predefinedValueOutcome);
      var objOutcome = Object.keys(valueParseOutcome);
      for (var itrOutcome = 0; itrOutcome < objOutcome.length; itrOutcome++) {
        if (valueParseOutcome) {
          outcomeOptionData.push(
            <option
              key={itrOutcome}
              value={valueParseOutcome[itrOutcome].value}
            />
          );
        }
      }
    }
    if (this.state.predefinedValueReviewType) {
      var valueParseReviewType = JSON.parse(
        this.state.predefinedValueReviewType
      );
      var objReviewType = Object.keys(valueParseReviewType);
      for (
        var itrReviewType = 0;
        itrReviewType < objReviewType.length;
        itrReviewType++
      ) {
        if (valueParseReviewType) {
          reviewTypeOptionData.push(
            <option
              key={itrReviewType}
              value={valueParseReviewType[itrReviewType].value}
            />
          );
        }
      }
    }
    if (this.state.predefinedValueActivity) {
      var valParse = JSON.parse(this.state.predefinedValueActivity);
      var obj2 = Object.keys(valParse);
      for (var itr = 0; itr < obj2.length; itr++) {
        if (valParse) {
          activityOptionData.push(
            <option key={itr} value={valParse[itr].value} />
          );
        }
      }
    }
    switch (key) {
      case "Case Type": {
        return (
          <div>
            <Input
              list="CaseType"
              name="CaseTypeNode"
              id="CaseTypeNode"
              value={val}
              autoComplete="off"
              bsSize={"sm"}
              className= { this.props.selectedNode.errorText &&((this.props.validateWorkflow && !val)|| val.length>maxLength )? "invalid":"valid"}
              onChange={this.handleInputChange}
              onBlur={this.checkPredefinedValue}
            />
            <datalist id="CaseType" className="datalist">{caseTypeOptionData}</datalist>
            {this.popOverData("CaseTypeNode")}
          </div>
        );
      }
      case "Activity Type":
        return (
          <div>
            <Input
              list="ActivityType"
              name="ActivityTypeNode"
              id="ActivityTypeNode"
              value={val}
              autoComplete="off"
              className= { this.props.selectedNode.errorText &&((this.props.validateWorkflow && !val)|| val.length>maxLength )? "invalid":"valid"}
              bsSize={"sm"}
              onChange={this.handleInputChange}
              onBlur={ this.checkPredefinedValue}
            />
            <datalist id="ActivityType">{activityTypeOptionData}</datalist>
            {this.popOverData("ActivityTypeNode")}
          </div>
        );
      case "Activity":
        return (
          <div>
            <Input
              list="Activity"
              name="ActivityNode"
              id="ActivityNode"
              value={val}
              bsSize={"sm"}
              autoComplete="off"
              className= { this.props.selectedNode.errorText &&((this.props.validateWorkflow && !val)|| val.length>maxLength )? "invalid":"valid"}
              onChange={this.handleInputChange}
              onBlur={ this.checkPredefinedValue}
            />
            <datalist id="Activity">{activityOptionData}</datalist>
            {this.popOverData("ActivityNode")}
          </div>
        );

      case "Outcome":
        return (
          <div>
            <Input
              list="Outcome"
              name="OutcomeNode"
              id="OutcomeNode"
              autoComplete="off"
              value={val}
              bsSize={"sm"}
              className= { this.props.selectedNode.errorText &&((this.props.validateWorkflow && !val) || val.length>maxLength )? "invalid":"valid"}
              onChange={this.handleInputChange}
              onBlur={this.checkPredefinedValue}
            />
            <datalist id="Outcome">{outcomeOptionData}</datalist>
            {this.popOverData("OutcomeNode")}
          </div>
        );

      case "Trigger Outcome":
        return (
          <div>
            <Input
              list="TriggerOutcome"
              name="triggerOutcomeNode"
              id="triggerOutcomeNode"
              value={val}
              autoComplete="off"
              bsSize={"sm"}
              onChange={this.handleInputChange}
              onBlur={this.checkPredefinedValue}
            />
            <datalist id="TriggerOutcome">{triggerOutcomeOptionData}</datalist>
            {this.popOverData("triggerOutcomeNode")}
          </div>
        );
      case "Case Category":
        return (
          <div>
            <Input
              list="CaseCategory"
              name="CaseCategoryNode"
              id="CaseCategoryNode"
              value={val}
              autoComplete="off"
              bsSize={"sm"}
              className= { this.props.selectedNode.errorText &&((this.props.validateWorkflow && !val)|| val.length>maxLength )? "invalid":"valid"}
              onChange={this.handleInputChange}
              onBlur={ this.checkPredefinedValue}
            />
            <datalist id="CaseCategory">{caseCategoryOptionData}</datalist>
{/*             {this.popOverData("CaseCategoryNode")}
 */}          </div>
        );

      case "Review Type":
        return (
          <div>
            <Input
              list="ReviewType"
              name="ReviewTypeNode"
              id="ReviewTypeNode"
              value={val}
              autoComplete= "off"
              bsSize={"sm"}
              className= { this.props.selectedNode.errorText &&((this.props.validateWorkflow && !val)|| val.length>maxLength )? "invalid":"valid"}
              onChange={this.handleInputChange}
              onBlur={ this.checkPredefinedValue}
            />
            <datalist id="ReviewType">{reviewTypeOptionData}</datalist>
            {this.popOverData("ReviewTypeNode")}
          </div>
        );
      case "Expedited Unit":
        return (
          <div>
            <Input
              type="select"
              value={val}
              bsSize={"sm"}
            autoComplete="off"
              className= { this.props.selectedNode.errorText &&(this.props.validateWorkflow && !val )? "invalid":"valid"}
              onChange={this.handleInputChange}
              onBlur={this.handleInputChange}
            >
              <option value="Select">Select</option>
              <option value="minutes">Minute(s)</option>
              <option value="hours">Hour(s)</option>
              <option value="workdays">Work day(s)</option>
              <option value="calendardays">Calendar day(s)</option>
              <option value="weeks">Week(s)</option>
              <option value="months">Month(s)</option>
              <option value="years">Year(s)</option>

            </Input>
          </div>
        );
      case "Standard Unit":
        return (
          <div>
            <Input
              type="select"
              value={val}
              bsSize={"sm"}
              autoComplete="off"
              onChange={this.handleInputChange}
              onBlur={this.handleInputChange}
              className= { this.props.selectedNode.errorText &&(this.props.validateWorkflow && !val )? "invalid":"valid"}

            >
              <option value="Select">Select</option>
              <option value="minutes">Minute(s)</option>
              <option value="hours">Hour(s)</option>
              <option value="workdays">Work day(s)</option>
              <option value="calendardays">Calendar day(s)</option>
              <option value="weeks">Week(s)</option>
              <option value="months">Month(s)</option>
              <option value="years">Year(s)</option>
            </Input>
          </div>
        );
         case "OActivity":
          return (
            <div>
              <Input
                list="OActivity"
                name="ActivityNode"
                id="ActivityNode"
                value={val}
                className= { this.props.selectedNode.errorText && val.length>maxLength ? "invalid":"valid"}
                bsSize={"sm"}
                autoComplete="off"
                onChange={this.handleInputChange}
                onBlur={ this.checkPredefinedValue}
              />
              <datalist id="OActivity">{activityOptionData}</datalist>
               {this.popOverData("ActivityNode")} 
            </div>
          );
  
      default:
        return (
          <div>
            <Input
              value={val}
              bsSize={"sm"}
              autoComplete="off"
              type="select"
              onChange={this.handleInputChange}
              onBlur={this.handleInputChange}
            ></Input>
          </div>
        );
    }
  }

  render() {
    let val = this.props.value;
    let type = this.props.type;
    let key = this.props.id;
    let maxLength = this.props.maxLength;
    return (
      <tr>
        <td>
          {!this.props.hidden
            ? !this.props.styleNode
              ? this.props.id === "Standard Unit" ||
                this.props.id === "Expedited Unit"
                ? ""
                : this.props.id === "OActivity" ? "Activity":this.props.id
              : ""
            : ""
            }
          {this.props.mandatory ? "*" : " "}
        </td>

        {!this.props.hidden ? (
          !this.props.styleNode ? (
            <td>
              {type === "select" ? (
                <Row>
                  <Col>
                    <FormGroup>
                      {this.getDataList(
                        key,
                        val,
                        maxLength,
                        this.props.getPredefinedValuesData
                      )}
                    </FormGroup>
                  </Col>
                </Row>
              ) : type === "number" ? (
                <Input
                  disabled={this.props.id === "key"}
                  value={val}
                  bsSize={"sm"}
                  autoComplete="off"
                  type="number"
                  className= {this.props.selectedNode.errorText && this.props.mandatory && this.props.validateWorkflow && !val ? "invalid":"valid"}
                  onChange={this.handleInputChange}
                  onBlur={this.handleInputChange}
                ></Input>
              ) : (
                <Input
                  disabled={this.props.id === "key"}
                  value={val}
                  bsSize={"sm"}
                  autoComplete="off"
                  type={type}
                  onChange={this.handleInputChange}
                  onBlur={this.handleInputChange}
                  className= {this.props.selectedNode.errorText && this.props.mandatory && this.props.validateWorkflow && !val ? "invalid":"valid"}
                ></Input>
              )}
            </td>
          ) : (
            <td></td>
          )
        ) : (
          <td></td>
        )}
      </tr>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCaseTypes: getCaseTypes,
      getPredefinedValues: getPredefinedValues,
      saveToPredefinedValueList: saveToPredefinedValueList,
    },
    dispatch
  );
}

function mapStateToProps({ workflowReducer }) {
  return {
    caseTypeOptionData: workflowReducer.caseTypeData,
    getPredefinedValuesData: workflowReducer.getPredefinedValues,
    saveToPredefinedListData: workflowReducer.saveToPredefinedList,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(InspectorRow)
);
