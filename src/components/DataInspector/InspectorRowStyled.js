import React, { Component } from "react";
import "./DataInspector.css";
import { Input } from "reactstrap";
import { FormGroup } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getCaseTypes } from "../../store/actions/sample";

 class InspectorRowStyled extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    this.props.onInputChange(
      this.props.id,
      e.target.value,
      e.type === "blur",
      this.props.type
    );
  }

  componentDidMount() {
       if(this.props.id === "Case Type"){
    this.props.getCaseTypes();
     }
  }

  render() {
    let val = this.props.value;
    let type = this.props.type;
    let key = this.props.id;
    var optionData = [];
    if (key === "Case Type") {
      if (this.props.caseTypeOptionData) {
        var obj2 = Object.keys(this.props.caseTypeOptionData);
        for (var itr = 0; itr < obj2.length; itr++) {
          optionData.push(
            <option key={itr} value={this.props.caseTypeOptionData[itr].case_type}>
              {this.props.caseTypeOptionData[itr].case_type}
            </option>
          );
        }
      }
    }
    return (
      <tr>
        <td>{!this.props.hidden ? this.props.styleNode ? this.props.id : "":""}</td>
        {!this.props.hidden?
          this.props.styleNode?
         (
          <td>
            {type === "select" ? (
              <FormGroup>
                <Input
                  disabled={this.props.id === "key"}
                  value={val}
                  bsSize={"sm"}
                  type="select"
                  onChange={this.handleInputChange}
                  onBlur={this.handleInputChange}
                >
                  <option>Select</option>
                  {optionData}
                </Input>
              </FormGroup>
            ) : (
              <Input
                disabled={this.props.id === "key"}
                value={val}
                bsSize={"sm"}
                type={type}
                onChange={this.handleInputChange}
                onBlur={this.handleInputChange}
              ></Input>
            )}
          </td>
        ) : (
          <td></td>
        ):<td></td>}
      </tr>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCaseTypes: getCaseTypes,
    },
    dispatch
  );
}

function mapStateToProps({ workflowReducer }) {
  return {
    caseTypeOptionData: workflowReducer.caseTypeData,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(InspectorRowStyled)
);
