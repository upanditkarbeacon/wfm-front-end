import * as React from "react";
import InspectorRow from "../DataInspector/InspectorRow";
import InspectorRowStyled from "../DataInspector/InspectorRowStyled";
import "./DataInspector.css";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import classnames from "classnames";
import nodeData from "../../constants/nodeData.json";

class SelectionInspector extends React.Component {
  /**
   * Render the object data, passing down property keys and values.
   */

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      activeTab: "1",
      caseTypeValue:""
    };
  }

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  componentDidMount(){
  }
  componentDidUpdate(prevProps){
  //  console.log("selection inslecgttor",this.props.selectedData)
  }
  
  getProperties(key) {
    const inputProperties = nodeData.properties[0];
    if (inputProperties[key]) {
      return inputProperties[key];
    } else {
      return "";
    }
  }

  renderObjectDetails(nodeType) {
    const selObj = this.props.selectedData;
    const dets = [];
    for (const k in selObj) {
      const val = selObj[k];
      var properties = this.getProperties(k);
      const type = properties.type;
      const hidden = properties.hidden;
      const styleNode = properties.styleNode;
      const mandatory =properties.mandatory;
      const maxLength =properties.maxLength;
      var row = "";
      if (nodeType === "node properties") {
        row = (
          <InspectorRow
            key={k}
            id={k}
            type={type}
            value={val}
            validateWorkflow={this.props.validateWorkflow}
            styleNode={styleNode}
            mandatory={mandatory}
            maxLength={maxLength ? maxLength: 256}
            selectedData  = {selObj[k]}
            showValidationFor={this.props.showValidationFor}
            selectedNode={selObj}
            nodeDataArray={this.props.nodeDataArray}
            hidden={hidden}
            onInputChange={this.props.onInputChange}
          />
        );
      }
      if (nodeType === "node style") {
        row = (
          <InspectorRowStyled
            key={k}
            id={k}
            type={type}
            value={val}
            styleNode={styleNode}
            hidden={hidden}
            onInputChange={this.props.onInputChange}
          />
        );
      }

      if (k === "key") {
        dets.unshift(row); // key always at start
      } else {
        dets.push(row);
      }
    }
    return dets;
  }

  render() {
    return (
      <div id="myInspectorDiv" className="inspector">
        <Nav tabs>
          <NavItem className="width-50">
            <NavLink
              className={classnames({ active: this.state.activeTab === "1" })}
              onClick={() => {
                this.toggle("1");
              }}
            
            >
             Properties
            </NavLink>
          </NavItem>
          <NavItem className="width-50">
            <NavLink
              className={classnames({ active: this.state.activeTab === "2" })}
              onClick={() => {
                this.toggle("2");
              }}
            >
               Style
            </NavLink>
          </NavItem>
        </Nav>

        <TabContent activeTab={this.state.activeTab} className="p-3">
          
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <table>
                  <tbody>{this.renderObjectDetails("node properties")}</tbody>
                </table>
              </Col>
            </Row>
          </TabPane>

          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                <table>
                  <tbody>{this.renderObjectDetails("node style")}</tbody>
                </table>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}
export default SelectionInspector;
