import * as go from "gojs";
import { ReactDiagram } from "gojs-react";
import * as React from "react";
import nodeData from "../../constants/nodeData.json";
import "gojs/extensions/Figures";
import "../../App.css";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getNodes } from "../../utility/getNodes";
import { getAllNodes } from "../../store/actions/sample";

import {
    linkProblemConverter
} from "../../utility/validateWorkflow";
import {
    getAllDraftWorkflowById,
    getExportedWorkflowById
} from "../../store/actions/workflow.action";
class ExportWorkflow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nodeDataValues: nodeData.nodeData,
            graphLinks: [],
            workflowDraft: [],
            nodeDataArray: [],
            linkDataArray: [],
        };

        // bind handler methods
        this.handleModelChange = this.handleModelChange.bind(this);
        this.diagramRef = React.createRef();
        this.initDiagram = this.initDiagram.bind(this);
    }


    componentDidMount() {
        
       // console.log(this.props.match)
        if (!this.diagramRef.current) return;
        const diagram = this.diagramRef.current.getDiagram();
        if (diagram instanceof go.Diagram) {
            //  diagram.addDiagramListener("ChangedSelection", this.props.onDiagramEvent);
        }
    }


    componentDidUpdate(prevProps) {
        const $ = go.GraphObject.make;

        const diagram = this.diagramRef.current.getDiagram();
        if (prevProps.nodeDataValues !== this.props.nodeDataValues) {
            console.log(this.props.nodeDataValues.data.node_data);
            getNodes(diagram, $, this.props.nodeDataValues.data.node_datata);
        }
        if (prevProps.exportedWorkflowData !== this.props.exportedWorkflowData) {
            var data = JSON.parse(this.props.exportedWorkflowData.data.workflowJson)
            if (data && data[0]) {
                this.setState({
                    nodeDataArray: data[0].nodeDataArray,
                    linkDataArray: data[0].linkDataArray
                })
            }
        }
    }

    /**
     * Get the diagram reference and remove listeners that were added during mounting.
     */
    componentWillUnmount() {
        if (!this.diagramRef.current) return;
        const diagram = this.diagramRef.current.getDiagram();
        if (diagram instanceof go.Diagram) {
            diagram.removeDiagramListener(
                "ChangedSelection",
                this.props.onDiagramEvent
            );
        }
    }

    async getValueForNodes($, diagram) {
        await this.props.getAllNodes();
        await this.props.getExportedWorkflowById(this.props.match.params.workflowId);
       // await this.props.getAllDraftWorkflowById(this.props.match.params.workflowId);
       // this.props.getAllNodes();
    }

    initDiagram() {
        const $ = go.GraphObject.make;
        const diagram = $(go.Diagram, {
            hoverDelay: 10,
            "undoManager.isEnabled": true,
            LinkDrawn: linkProblemConverter,
            model: $(go.GraphLinksModel, {
                linkLabelKeysProperty: "labelKeys",
                linkKeyProperty: "key", // IMPORTANT! must be defined for merges and data sync when using GraphLinksModel
                // positive keys for nodes
                makeUniqueKeyFunction(m, data) {
                    let k = data.key || 1;
                    while (m.findNodeDataForKey(k)) k++;
                    data.key = k;
                    return k;
                },
                // negative keys for links
                makeUniqueLinkKeyFunction: (m, data) => {
                    let k = data.key || -1;
                    while (m.findLinkDataForKey(k)) k--;
                    data.key = k;
                    return k;
                },
            }),
        });
        diagram.scrollMode = diagram.InfiniteScroll;
        diagram.model.isReadOnly = true;
        diagram.allowTextEdit = false;
        diagram.toolManager.linkingTool.archetypeLabelNodeData = diagram.addDiagramListener(
            "ExternalObjectsDropped",
            function (e) {
                e.subject.each(function (node) {
                    var model = e.diagram.model;
                    model.setDataProperty(node.data, "text", "");
                });
            }
        );


        diagram.linkTemplate = $(
            go.Link,
            { toShortLength: 4 },
            {
                routing: go.Link.AvoidsNodes,
                curve: go.Link.JumpOver,
                corner: 5, toShortLength: 4,
                relinkableFrom: true,
                relinkableTo: true,
                reshapable: true,
                resegmentable: true,
                selectionAdorned: false
            },
            $(
                go.Shape,
                new go.Binding("stroke", "isHighlighted", function (h) {
                    return h ? "red" : "grey";
                }).ofObject(),
                new go.Binding("strokeWidth", "isHighlighted", function (h) {
                    return h ? 2 : 2;
                }).ofObject()
            ),
            $(
                go.Shape,
                { toArrow: "Standard", strokeWidth: 0 },
                new go.Binding("fill", "isHighlighted", function (h) {
                    return h ? "red" : "grey";
                }).ofObject()
            )
        );
        this.getValueForNodes($, diagram);

        return diagram;
    }
    handleModelChange = (obj) => {

    }

    render() {
        return (
            <ReactDiagram
                ref={this.diagramRef}
                divClassName="myDiagramDiv myDiagramExport"
                className="myDiagramExport"
                initDiagram={this.initDiagram}
                nodeDataArray={this.state.nodeDataArray}
                linkDataArray={this.state.linkDataArray}
                modelData={this.props.modelData}
                onModelChange={this.handleModelChange}
                skipsDiagramUpdate={this.props.skipsDiagramUpdate}
            />
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getAllDraftWorkflowById: getAllDraftWorkflowById,
            getAllNodes: getAllNodes,
            getExportedWorkflowById:getExportedWorkflowById
        },
        dispatch
    );
}
function mapStateToProps({ workflowReducer }) {
    return {
        getWorkflowById: workflowReducer.getWorkflowDraftById,
        nodeDataValues: workflowReducer.nodeDataArray,
        exportedWorkflowData:workflowReducer.exportedWorkflow
    };
}
export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(ExportWorkflow)
);
