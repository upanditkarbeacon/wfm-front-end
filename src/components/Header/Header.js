import React, { Component } from "react";
import { Navbar, Nav, Form, Row, Col } from "react-bootstrap";
import { Dropdown, DropdownToggle, DropdownMenu} from "reactstrap";
import { Button } from "reactstrap";
import ViewDraftWorkflow from "../Workflow/viewDraftWorkflow";
import {
  saveWorkflowAsDraft,
  getAllDraftWorkflows,
  publishWorkflow,
  detectCycle,
  checkImpactedList
} from "../../store/actions/workflow.action";

import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Message from "../Message/Message";
import WarningMessage from "../Message/WarningMessage";
import history from "./../../history";
import "./Header.css";
import { toast } from "react-toastify";
import {
  validateWorkflow,
  highlightLoops,
  clearErrorMessage,
  checkImmidieateLoops,
  getReference
} from "../../utility/validateWorkflow";
import ChangePassword from "../User/ChangePassword/ChangePassword";
import { confirmationMessages, errorMessages } from "../../constants/Constants";
import WorkflowLink from "../Workflow/WorkflowLink";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      digramInstant: {},
      workflowDraft: {},
      workflowId: "",
      isPublished: false,
      showMessage: false,
      showWarningMessage: false,
      showSaveMessage: false,
      loopsIgnores: false,
      workflowHasLoops: false,
      dropdownOpen: false,
      messageData: {},
      saveMessageData: {},
      dropdownExportOpen:false,
      disableExport:true,
      warningMessageData: {},
      error: {
        workflowNameError: "",
      },
      workflowName: "",
      linkDataArray: [],
      nodeDataArray: [],
      modalShow: false,
      modalShowDraftWorkflow: false,
      modalShowLinkWorkflow: false,
      modalShowChangePassword: false,
    };
    this.export = this.export.bind(this);
    this.toggle = this.toggle.bind(this);
    this.detectCycle = this.detectCycle.bind(this);
    this.validate = this.validate.bind(this);
    this.saveAsDraft = this.saveAsDraft.bind(this);
    this.confirmToSave = this.confirmToSave.bind(this);
    this.confirmToPublish = this.confirmToPublish.bind(this);
    this.publish = this.publish.bind(this);
    this.checkImpactedList = this.checkImpactedList.bind(this)
  }

  setModalShow(modalshow) {
    this.setState({
      modalShow: modalshow,
    });
  }
  toggleDropDown = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };

  toggleExport = () => {
    this.setState({
      dropdownExportOpen: !this.state.dropdownExportOpen,
    });
  };

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  setModalShowChangePassword(modalShowChangePassword) {
    this.setState({
      modalShowChangePassword: modalShowChangePassword,
    });
  }

  onClickSvg=()=>{
    
    var svg = getReference().makeSvg({ scale: 1, background: "white" });
      var svgstr = new XMLSerializer().serializeToString(svg);
      var blob = new Blob([svgstr], { type: "image/svg+xml" });
      this.myCallback(blob);
  
  }
   myCallback=(blob) =>{
    var url = window.URL.createObjectURL(blob);
    var filename = "mySVGFile.svg";

    var a = document.createElement("a");
    a.style = "display: none";
    a.href = url;
    a.download = filename;

    // IE 11
    if (window.navigator.msSaveBlob !== undefined) {
      window.navigator.msSaveBlob(blob, filename);
      return;
    }

    document.body.appendChild(a);
    requestAnimationFrame(function() {
      a.click();
      window.URL.revokeObjectURL(url);
      document.body.removeChild(a);
    });
  }
  setModalShowDraftWorkflow(modalShowDraftWorkflow) {
    this.setState({
      modalShowDraftWorkflow: modalShowDraftWorkflow,
    });
    this.props.getAllDraftWorkflows();
  }

  setModalShowMailWorkflow=(modalShowLinkWorkflow)=> {
    this.setState({
      modalShowLinkWorkflow: modalShowLinkWorkflow,
    });
  }

  closeShowMessage = () => {
    this.setState({ showMessage: false });
  };

  closeSaveShowMessage = () => {
    this.setState({ showSaveMessage: false });
  };
  closeWarningShowMessage = () => {
    this.setState({ showWarningMessage: false });
  };
  confirmToSave() {
    const validateError = this.validate();
    if (!validateError) {
      this.setState({
        showSaveMessage: true,
        isPublished: false,
        saveMessageData: {
          type: "success",
          message: confirmationMessages.SAVE_WORKFLOW,
        },
      });
    }
  }
  saveAsDraft() {
    clearErrorMessage();
    const validateError = this.validate();
    if (!validateError) {
      this.props.showLoader()
      this.setState({
        showSaveMessage:false
      })
      if (this.state.workflowId) {
        var workflowData = {
          workflowId: this.state.workflowId,
          workflowName: this.state.workflowName,
          userId: this.state.userId,
          nodeDataArray: this.state.nodeDataArray,
          linkDataArray: this.state.linkDataArray,
        };
        this.props.saveWorkflowAsDraft(workflowData);
      } else {
        var workflowDataSave = {
          workflowName: this.state.workflowName,
          userId: this.state.userId,
          nodeDataArray: this.state.nodeDataArray,
          linkDataArray: this.state.linkDataArray,
        };
        this.props.saveWorkflowAsDraft(workflowDataSave);
      }
    }
  }

  OnPropertyChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  async detectCycle() {
    var workflowData = {
      workflowId: this.state.workflowId,
      workflowName: this.state.workflowName,
      userId: "",
      nodeDataArray: this.state.nodeDataArray,
      linkDataArray: this.state.linkDataArray,
    };
    await this.props.detectCycle(workflowData);
  }

  export() {
    var fileName = "workflow.json";
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    var json = JSON.stringify(this.state.workflowDraft),
      blob = new Blob([json], { type: "octet/stream" }),
      url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  }
  validate = () => {
    let isError = false;
    let tempError = this.state.error;
    if (
      !this.state.workflowName ||
      (this.state.workflowName &&
        !this.state.workflowName.replace(/\s/g, "").length)
    ) {
      isError = true;
      tempError.workflowNameError = errorMessages.WORKFLOW_NAME;
    } else {
      tempError.workflowNameError = "";
    }

    if (isError) {
      this.setState({
        error: tempError,
      });
    }
    return isError;
  };

  componentDidMount() {
    const loginUser = JSON.parse(localStorage.getItem("loginUser"));
    this.setState({
      username: loginUser.userName,
      userId: loginUser.userId,
    });
  }

  validateWorkflow = () => {
    const validateError = this.validate();
    const isValidWorkflow = validateWorkflow();
    this.props.validateWorkflow(isValidWorkflow);
    if (!isValidWorkflow && !validateError) {
      toast.success(errorMessages.NO_ERROR, {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    }
  };

  fixLoop = () => {
    highlightLoops(this.state.loopData);
    this.setState({
      loopsIgnores: false,
    });
  };
  continueWithLoop = () => {
    this.setState({
      showWarningMessage: false,
      showMessage: true,
      isPublished: true,
      loopsIgnores: true,
      messageData: {
        type: "success",
        message: confirmationMessages.PUBLISH_WORKFLOW ,
        impactedCase:this.state.impactedListNumber ? "Impacted Cases : "+this.state.impactedListNumber:"",
      },
    });
  };

 async  checkImpactedList(){
    if(this.state.nodeDataArray){
      for(var i in this.state.nodeDataArray){
        if(this.state.nodeDataArray[i]["category"] === "Case Type"){
          var data={
            caseType: this.state.nodeDataArray[i]["Case Type"],
            caseCategory: this.state.nodeDataArray[i]["Case Category"]
          }
         await  this.props.checkImpactedList(data );
        }
      }
    }
  }
  async confirmToPublish() {
    await this.detectCycle();
    await this.checkImpactedList();
    const validateError = this.validate();
    const isValidWorkflow = validateWorkflow();
    this.props.validateWorkflow(isValidWorkflow);
    if (!isValidWorkflow && !validateError) {
      if (this.state.workflowHasLoops) {
        this.setState({
          showWarningMessage: true,
          isPublished: true,
          warningMessageData: {
            type: "warning",
            message: errorMessages.WARNING_LOOP_DETETCTED,
          },
        });
      } else {
        this.continueWithLoop();
      }
    }
  }

  publish = () => {
    this.setState({
      isPublished: true,
      showMessage:false
    });
    this.props.showLoader()
    this.saveAsDraft();
  };

  getDiagram(content) {
    this.props.importDiagram(content);
  }

  componentDidUpdate(prevProps) {
    
    if(prevProps.impactedListInfo !==this.props.impactedListInfo){
      if(this.props.impactedListInfo.status === 200 || this.props.impactedListInfo.status===201){
          this.setState({
              impactedListNumber:this.props.impactedListInfo.data
          })
      }
    }
    if (prevProps.detectCycleInfo !== this.props.detectCycleInfo) {
      if (
        this.props.detectCycleInfo.status === 200 ||
        this.props.detectCycleInfo.status === 201
      ) {
        if (
          this.props.detectCycleInfo.data &&
          this.props.detectCycleInfo.data.length > 0
        ) {

          if (
          (this.props.detectCycleInfo.data[0] &&
            Object.entries(this.props.detectCycleInfo.data[0]).length > 0)
          ) {
            checkImmidieateLoops(false);
            this.setState({
              loopData: this.props.detectCycleInfo.data,
              workflowHasLoops: true,
            });
          }
          else if(checkImmidieateLoops(false)){
            this.setState({
              loopData: this.props.detectCycleInfo.data,
              workflowHasLoops: true,
            });
          }
          else {
            this.setState({
              workflowHasLoops: false,
            });
          }
        }
      }
    }
    if (prevProps.publishedWorkflowInfo !== this.props.publishedWorkflowInfo) {
      if (
        this.props.publishedWorkflowInfo.status === 200 ||
        this.props.publishedWorkflowInfo === 201
      ) {
        this.closeShowMessage();
        this.props.hideLoader()
        toast.success(
          confirmationMessages.WORKFLOW_TEXT +
            this.state.workflowName +
            confirmationMessages.PUBLISHED_SUCCESS,
          {
            position: toast.POSITION.BOTTOM_RIGHT,
          }
        );
      } else if (
        this.props.publishedWorkflowInfo &&
        this.props.publishedWorkflowInfo.response &&
        this.props.publishedWorkflowInfo.response.status === 500
      ) {
        this.props.hideLoader()
        toast.error(errorMessages.WORKFLOW_PUBLISH_FAIL, {
          position: toast.POSITION.BOTTOM_RIGHT,
        });
      }
      else{
        this.props.hideLoader()
      }
    }
    if (prevProps.saveWorkflow !== this.props.saveWorkflow) {
      if (
        this.props.saveWorkflow.status === 200 ||
        this.props.saveWorkflow.status === 201
      ) {
            this.setState({
              workflowId:this.props.saveWorkflow.data.workflowId,
              disableExport:false,
            })
        if (this.props.saveWorkflow.data) {
          if (this.state.isPublished) {
            this.props.publishWorkflow(
              this.props.saveWorkflow.data.workflowId,
              this.props.saveWorkflow.data.workflowJson
            );
          }
        }
        this.closeShowMessage();
        this.closeSaveShowMessage();
        this.props.hideLoader();
        toast.success(
          confirmationMessages.WORKFLOW_TEXT +
            this.state.workflowName +
            confirmationMessages.SAVED_SUCCESS,
          {
            position: toast.POSITION.BOTTOM_RIGHT,
          }
        );
      }
      else if (
        this.props.saveWorkflow &&
        this.props.saveWorkflow.response &&
        this.props.saveWorkflow.response.status === 400
      ) {

        this.closeSaveShowMessage();
        this.props.hideLoader();
        toast.error(
          confirmationMessages.WORKFLOW_TEXT + this.state.workflowName+
            errorMessages.DUPLICATE_WORKFLOW,
          {
            position: toast.POSITION.BOTTOM_RIGHT,
          }
        );
      }
      else{
        this.closeSaveShowMessage();
        this.props.hideLoader();
        toast.error(
            errorMessages.WORKFLOW_SAVE_FAIL,
          {
            position: toast.POSITION.BOTTOM_RIGHT,
          }
        );
      }
    }


    if (prevProps.workflowName !== this.props.workflowName) {
      this.setState({
        workflowName: this.props.workflowName,
      });
    }

    if (prevProps.digramInstant !== this.props.digramInstant) {
      this.setState({
        digramInstant: this.props.digramInstant,
      });
    }

    if (prevProps.nodeDataArray !== this.props.nodeDataArray) {
      this.setState({
        workflowDraft: [
          {
            nodeDataArray: this.props.nodeDataArray,
            linkDataArray: this.props.linkDataArray,
          },
        ],
      });
      this.setState({
        nodeDataArray: this.props.nodeDataArray,
        linkDataArray: this.props.linkDataArray,
      });
    }
    if (prevProps.linkDataArray !== this.props.linkDataArray) {
      this.setState({
        workflowDraft: [
          {
            nodeDataArray: this.props.nodeDataArray,
            linkDataArray: this.props.linkDataArray,
          },
        ],
      });
      this.setState({
        nodeDataArray: this.props.nodeDataArray,
        linkDataArray: this.props.linkDataArray,
      });
    }
  }

  onClickLogout = () => {
    localStorage.removeItem("loginUser");
    history.push("/login");
  };
  getWorkflowId(e) {
    this.setState({
      workflowId: e,
      disableExport:false,

    });
  }
  getWorkflowName(e) {
    this.setState({
      workflowName: e,
    });
  }
  render() {
    const { workflowName } = this.state;

    return (
      <div className="main-header">
        <div className="header">   
          <Navbar variant="dark">
            <Navbar.Brand href="#home">
              Workflow Automation Manager
            </Navbar.Brand>
            <Nav className="mr-auto">
              <Nav></Nav>
              {/*  <Nav.Link href="#features">Features</Nav.Link>
          <Nav.Link href="#pricing">Pricing</Nav.Link> */}
            </Nav>
            <Dropdown
              isOpen={this.state.dropdownOpen}
              toggle={this.toggleDropDown}
            >
              <DropdownToggle
                tag="span"
                data-toggle="dropdown"
                aria-expanded={this.state.dropdownOpen}
              >
                <span className=" userDiv d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                  <i className=" lni lni-user mr-2"></i>
                  <span className="px-1 mr-lg-2 ml-2 ml-lg-0">
                    {this.state.username}
                  </span>
                </span>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-left">
                <div
                  className="dropdown-item"
                  onClick={this.onClickChangePassword}
                >
                  <div onClick={() => this.setModalShowChangePassword(true)}>
                    <i className="lni lni-eye text-danger"></i> Change Password
                  </div>
                </div>
                <div className="dropdown-item" onClick={this.onClickLogout}>
                  <i className="lni lni-shift-right text-danger"></i> Log Out
                </div>
              </DropdownMenu>
            </Dropdown>
          </Navbar>
        </div>
        <div className="header2 d-flex justify-content-end">
          
          <Navbar
            collapseOnSelect
            expand="lg"
            bg="light"
            variant="light"
            className="w-100"
          >
        <Button onClick={() =>this.setModalShowMailWorkflow(true)} disabled={this.state.disableExport}>Export</Button>
           {/*  <Dropdown group isOpen={this.state.dropdownExportOpen} size="sm" toggle={this.toggleExport}>
                        <DropdownToggle caret>
                          Export
                        </DropdownToggle>
                        <DropdownMenu>
                        <DropdownItem onClick={this.onClickSvg}>SVG</DropdownItem>
                        </DropdownMenu>
                      </Dropdown>
            */} <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto"></Nav>
              <Nav>
                <Navbar href="#">
                  <Form>
                    <Form.Row>
                      <Form.Group
                        as={Row}
                        controlId="formHorizontalEmail"
                        className="mb-0 mr-2 workflowNameInput"
                      >
                        <Form.Label column sm={5} className="text-right">
                          Workflow Name
                        </Form.Label>
                        <Col sm={7}>
                          <Form.Control
                            type="text"
                            name="workflowName"
                            onChange={this.OnPropertyChange}
                            value={workflowName}
                            autoComplete="off"
                            className={
                              "inputerror " +
                              (this.state.error.workflowNameError
                                ? "invalid"
                                : "valid")
                            }
                            placeholder="Type name here"
                          />
                          <span className="errorMessage">
                            {this.state.error.workflowNameError}
                          </span>
                        </Col>
                      </Form.Group>
                    </Form.Row>
                  </Form>
                </Navbar>
                <Nav.Link href="">
                  <Button color="info" onClick={this.confirmToSave}>
                    Save As Draft
                  </Button>
                  <Button
                    color="info"
                    className="ml-3"
                    onClick={() => this.setModalShowDraftWorkflow(true)}
                  >
                    View Workflow
                  </Button>
                  <Button
                    color="info"
                    className="ml-3"
                    onClick={this.validateWorkflow}
                  >
                    Validate
                  </Button>
                  <Button
                    color="primary"
                    className="ml-3"
                    onClick={this.confirmToPublish}
                  >
                    Publish
                  </Button>
                </Nav.Link>
                {/* <Nav.Link eventKey={2} href="#memes">
        Dank memes
      </Nav.Link> */}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
        <ChangePassword
          show={this.state.modalShowChangePassword}
          onHide={() => this.setModalShowChangePassword(false)}
        ></ChangePassword>
    
        <ViewDraftWorkflow
          getDiagram={(e) => this.getDiagram(e)}
          show={this.state.modalShowDraftWorkflow}
          workflowName={(e) => this.getWorkflowName(e)}
          workflowId={(e) => this.getWorkflowId(e)}
          onHide={() => this.setModalShowDraftWorkflow(false)}
        ></ViewDraftWorkflow>

         <WorkflowLink
          getDiagram={(e) => this.getDiagram(e)}
          show={this.state.modalShowLinkWorkflow}
          workflowName={(e) => this.getWorkflowName(e)}
          workflowId={this.state.workflowId}
          onHide={() => this.setModalShowMailWorkflow(false)}
        ></WorkflowLink>
        {this.state.showMessage ? (
          <Message
            msgIsOpen={this.state.showMessage}
            msgIsClose={this.closeShowMessage}
            messageData={this.state.messageData}
            buttonValue="Publish"
            action={(event) => this.publish()}
          ></Message>
        ) : (
          ""
        )}

        {this.state.showSaveMessage ? (
          <Message
            msgIsOpen={this.state.showSaveMessage}
            msgIsClose={this.closeSaveShowMessage}
            messageData={this.state.saveMessageData}
            buttonValue="Save"
            action={(event) => this.saveAsDraft(event)}
          ></Message>
        ) : (
          ""
        )}

        {this.state.showWarningMessage ? (
          <WarningMessage
            msgIsOpen={this.state.showWarningMessage}
            msgIsClose={this.closeWarningShowMessage}
            warningMessageData={this.state.warningMessageData}
            fixLoopAction={this.fixLoop}
            action={(event) => this.continueWithLoop()}
          ></WarningMessage>
        ) : (
          ""
        )}
          
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveWorkflowAsDraft: saveWorkflowAsDraft,
      getAllDraftWorkflows: getAllDraftWorkflows,
      publishWorkflow: publishWorkflow,
      detectCycle: detectCycle,
      checkImpactedList:checkImpactedList
    },
    dispatch
  );
}
function mapStateToProps({ workflowReducer }) {
  return {
    saveWorkflow: workflowReducer.saveWorkflowAsDraft,
    publishedWorkflowInfo: workflowReducer.publishedWorkflow,
    detectCycleInfo: workflowReducer.detectCycleInWorkflow,
    impactedListInfo: workflowReducer.impactedList
  };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
