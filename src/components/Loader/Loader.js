import React from "react";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "./loader.css";
export default class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      done: undefined,
    };
  }

  render() {
    return (
      <div>
        <Loader
          type="Oval"
          color="Gray"
          height={50}
          className="content"
          width={50}
        />
{/*         <h6 className="content">Loading</h6>
 */}      </div>
    );
  }
}
