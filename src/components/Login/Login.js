import React, { Component } from "react";
import "./Login.scss";
import { Container } from "react-bootstrap";
import { login } from "../../store/actions/login.action";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import { errorMessages } from "../../constants/Constants";
import history from "./../../history";
import logo from "./logo.png";
import Loader from "../Loader/Loader"

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      serverError: "",
      showLoader:false,
      error: {
        userNameError: "",
        passwordError: "",
      },
    };
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  forgotPassword = () => {
    history.push("/forgotPassword");
  };
  validate = () => {
    let isError = false;
    let tempError = this.state.error;
    if (!this.state.userName) {
      isError = true;
      tempError.userNameError = errorMessages.EMAIL_ERROR;
    } else if (
      this.state.userName &&
      !this.state.userName.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    ) {
      isError = true;
      tempError.userNameError = errorMessages.INVALID_EMAIL;
    } else {
      tempError.userNameError = "";
    }

    if (!this.state.password) {
      isError = true;
      tempError.passwordError = errorMessages.PASSWORD_ERROR;
    } else {
      tempError.passwordError = "";
    }
    if (isError) {
      this.setState({
        error: tempError,
      });
    }
    return isError;
  };
  onLogin = (e) => {
    e.preventDefault();
    this.setState({
      serverError:""
    })
    var valid = this.validate();
    if (!valid) {
      this.setState({
        showLoader : true
      })
      var userData = {
        emailId: this.state.userName,
        password: this.state.password,
      };
      this.props.login(userData);
    }

    // history.push("/Dashboard")
  };

  async componentDidUpdate(prevProps) {
    const { loginInfo } = this.props;
    if (prevProps.loginInfo !== loginInfo) {
      if (loginInfo.status === 200 || loginInfo.status === 201) {
        this.setState({
          showLoader : false
        })
        let loginUser = {
          email: loginInfo.data.emailId,
          userName: loginInfo.data.username,
          userId: loginInfo.data.userId,
        };
        await localStorage.setItem("loginUser", JSON.stringify(loginUser));
        history.push("/Dashboard");
      } else if (
        loginInfo &&
        loginInfo.response &&
        loginInfo.response.status === 401
      ) {
        this.setState({
          showLoader : false
        })
        if (loginInfo.response.data) {
          this.setState({
            serverError: loginInfo.response.data,
          });
        }
      }
      else{
        this.setState({
          showLoader : false,
          serverError: "Something went wrong",

        })
        console.log("server error")
      }
    }
  }
  render() {
    const { userName, password } = this.state;
    return (
      <Container fluid className="login-border">
        <div className="my-3 d-flex align-items-center justify-content-center">
            <img src={logo} alt="My logo" className="logo" />
          </div>
        <div className="d-flex align-items-center justify-content-center login-wrapper">
    {/*       <div className="my-3">
            <img src={logo} alt="My logo" />
          </div> */}
          <form className="col-sm-10 col-lg-4 login-box">
            <h4 className="align-center">Login</h4>
            <div className="form-group">
              <label>Username</label>
              <input
                type="text"
                name="userName"
                value={userName}
                onChange={this.onChange}
                className={
                  "form-control" +
                  (this.state.error.userNameError ? " invalid" : " valid")
                }
                placeholder="Enter username"
              />
              <span className="errorMessage">
                {this.state.error.userNameError}
              </span>
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                name="password"
                className={
                  "form-control " +
                  (this.state.error.passwordError ? " invalid" : "valid")
                }
                value={password}
                onChange={this.onChange}
                placeholder="Enter password"
              />
              <span className="errorMessage">
                {this.state.error.passwordError}
              </span>
              <span className="errorMessage">{this.state.serverError}</span>
            </div>

            {/*               <div className="form-group">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                            </div>
                        </div> */}
            <button
              type="submit"
              onClick={this.onLogin}
              className="btn btn-primary btn-block login"
            >
              Login
            </button>

            <p
              className="forgot-password text-center mt-3"
              onClick={this.forgotPassword}
            >
              Forgot password?
            </p>
          </form>
        </div>
        {this.state.showLoader ? 
        <Loader/>:<div></div>}
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      login: login,
    },
    dispatch
  );
}
function mapStateToProps({ loginReducer }) {
  return {
    loginInfo: loginReducer.loginData,
  };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
