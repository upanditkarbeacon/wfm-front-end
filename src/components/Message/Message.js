import React, { Component } from "react";
import Modal from "react-awesome-modal";
import { Button } from "reactstrap";
class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  componentWillUnmount() {
    this.props.msgIsClose();
  }

  closeModal = () => {
    this.props.msgIsClose();
  };

  render() {
    return (
      <div>
        <Modal visible={this.props.msgIsOpen} width="400" effect="fadeInDown">
          <div>
            <div
              className={
                this.props.messageData.type === "success"
                  ? "modal-content alertmessage success-alert "
                  : this.props.messageData.type === "error"
                  ? "modal-content alertmessage error-alert "
                  : this.props.messageData.type === "info"
                  ? "modal-content alertmessage info-alert "
                  : ""
              }
            >
              {!this.props.hideCancel ?
              <div className="modal-header">Confirm</div>: <div className="modal-header"></div>}
              <div className="modal-body">{this.props.messageData.message}
              <div><b>{this.props.messageData.impactedCase}</b></div>
              </div>

              <div className="modal-footer">
                <Button
                  color="primary"
                  size="sm"
                  value={this.props.messageData}
                  onClick={(event) => this.props.action(this.props.messageData)}
                >
                  {this.props.buttonValue}
                </Button>
                {!this.props.hideCancel ?
                <Button
                  color="danger"
                  size="sm"
                  className="btn btn-danger onlyokbutton"
                  onClick={(event) => this.closeModal()}
                >
                  Cancel
                </Button>:<div></div>}
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default Messages;
