import React, { Component } from "react";
import Modal from "react-awesome-modal";
import { Button } from "reactstrap";
class WarningMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  componentWillUnmount() {
    this.props.msgIsClose();
  }

  closeModal = () => {
    this.props.fixLoopAction()

    this.props.msgIsClose();

};

  render() {
    return (
      <div>
        <Modal visible={this.props.msgIsOpen} width="400" effect="fadeInDown">
          <div>
            <div
              className={
                this.props.warningMessageData.type === "success"
                  ? "modal-content alertmessage success-alert "
                  : this.props.warningMessageData.type === "error"
                  ? "modal-content alertmessage error-alert "
                  : this.props.warningMessageData.type === "info"
                  ? "modal-content alertmessage info-alert "
                  : ""
              }
            >
              <div className="modal-header">Warning</div>
                <div className="modal-body">{this.props.warningMessageData.message}</div>
              <div className="modal-footer">
                <Button
                  color="warning"
                  size="sm"
                  value={this.props.warningMessageData}
                  onClick={(event) => this.props.action()}
                >
                  Continue With Loop
                </Button>
                {!this.props.hideCancel ?
                <Button
                  color="primary"
                  size="sm"
                  className="btn btn-danger onlyokbutton"
                  onClick={(event) => this.closeModal()}
                >
                  Fix Loop
                </Button>:<div></div>}
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default WarningMessage;
