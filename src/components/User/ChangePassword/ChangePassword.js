import React, { Component } from "react";
import { Button, Row, FormGroup, Input } from "reactstrap";
import { bindActionCreators } from "redux";
import { changePassword } from "../../../store/actions/login.action";
import { withRouter } from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import { Modal } from "react-bootstrap";
import "./changePassword.css";
import { toast } from "react-toastify";
import history from "../../../history";
import {
  errorMessages,
  confirmationMessages,
} from "../../../constants/Constants";

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword: "",
      confirmNewPassword: "",
      serverError: "",
      error: {
        oldPasswordError: "",
        newPasswordError: "",
        confirmNewPasswordError: "",
      },
    };
  }
  validate = () => {
    let isError = false;
    let tempError = this.state.error;
    if (!this.state.oldPassword) {
      isError = true;
      tempError.oldPasswordError = errorMessages.OLD_PASSWORD_ERROR;
    } else {
      tempError.oldPasswordError = "";
    }
    if (!this.state.newPassword) {
      isError = true;
      tempError.newPasswordError = errorMessages.NEW_PASSWORD_ERROR;
    } else if (this.state.newPassword && this.state.newPassword.length < 6) {
      tempError.newPasswordError = errorMessages.PASSWORD_LENGTH_ERROR;
    } else {
      tempError.newPasswordError = "";
    }

    if (!this.state.confirmNewPassword) {
      isError = true;
      tempError.confirmNewPasswordError = errorMessages.CONFRIM_PASSWORD_ERROR;
    } else {
      tempError.confirmNewPasswordError = "";
    }
    if (this.state.newPassword && this.state.confirmNewPassword) {
      if (this.state.newPassword !== this.state.confirmNewPassword) {
        isError = true;
        tempError.confirmNewPasswordError =
          errorMessages.MATCH_CONFIRM_PASSWORD;
      }
    }
    if (isError) {
      this.setState({
        error: tempError,
      });
    }
    return isError;
  };

  changeToLogin = () => {
    history.push("/login");
  };

  componentDidMount() {
    const loginUser = JSON.parse(localStorage.getItem("loginUser"));
    this.setState({
      emailId: loginUser.email,
    });
  }

  componentDidUpdate(prevProps) {
    const { changePasswordInfo } = this.props;
    if (prevProps.show !== this.props.show) {
      this.setState({
        error: {},
        serverError: "",
      });
    }
    if (prevProps.changePasswordInfo !== changePasswordInfo) {
      if (
        changePasswordInfo &&
        (changePasswordInfo.status === 200 || changePasswordInfo.status === 201)
      ) {
        localStorage.removeItem("loginUser");
        this.changeToLogin();
        toast.success(confirmationMessages.PASSWORD_CHANGE_SUCCESS, {
          position: toast.POSITION.BOTTOM_RIGHT,
        });
      } else if (
        changePasswordInfo &&
        changePasswordInfo.response &&
        changePasswordInfo.response.status === 417
      ) {
        if (changePasswordInfo.response.data) {
          this.setState({
            serverError: changePasswordInfo.response.data,
          });
        }
      }
    }
    if (!this.props.onHide) {
      this.setState({
        error: {},
      });
    }
  }

  onChangeProperty = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  onChangePassword = (e) => {
    e.preventDefault();
    var valid = this.validate();
    if (!valid) {
      var userData = {
        emailId: this.state.emailId,
        currentPassword: this.state.oldPassword,
        newPassword: this.state.newPassword,
      };
      this.props.changePassword(userData);
    }
    // history.push("/Dashboard")
  };

  render() {
    const { oldPassword, newPassword, confirmNewPassword } = this.state;
    return (
      <Modal
        show={this.props.show}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Change Password
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <FormGroup className="changePassword">
              <label for="password">Old Password</label>
              <Input
                id="oldPassword"
                type="password"
                name="oldPassword"
                value={oldPassword}
                onChange={this.onChangeProperty}
                className={
                  "form-control" +
                  (this.state.error.oldPasswordError ? " invalid" : " valid")
                }
                placeholder="Enter old password"
              />
              <span className="errorMessage">
                {this.state.error.oldPasswordError}
              </span>

              <label for="password">New Password</label>
              <Input
                id="newPassword"
                type="password"
                name="newPassword"
                value={newPassword}
                onChange={this.onChangeProperty}
                className={
                  "form-control" +
                  (this.state.error.newPasswordError ? " invalid" : " valid")
                }
                placeholder="Enter new password"
              />
              <span className="errorMessage">
                {this.state.error.newPasswordError}
              </span>
              <label for="password">Confirm Password</label>
              <Input
                id="confirmNewPassword"
                type="password"
                name="confirmNewPassword"
                value={confirmNewPassword}
                onChange={this.onChangeProperty}
                className={
                  "form-control" +
                  (this.state.error.confirmNewPasswordError
                    ? " invalid"
                    : " valid")
                }
                placeholder="Enter new password"
              />
              <span className="errorMessage">
                {this.state.error.confirmNewPasswordError}
              </span>
              {this.state.serverError ? (
                <span className="errorMessage">{this.state.serverError}</span>
              ) : (
                <div></div>
              )}
            </FormGroup>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button color="primary" onClick={this.onChangePassword}>
            Submit
          </Button>
          <Button color="secondary" onClick={this.props.onHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      changePassword: changePassword,
    },
    dispatch
  );
}
function mapStateToProps({ loginReducer }) {
  return {
    changePasswordInfo: loginReducer.changePassword,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ChangePassword)
);
