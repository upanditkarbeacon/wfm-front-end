import React, { Component } from "react";
import "../../Login/Login.scss";
import { Container } from "react-bootstrap";
import { forgotPassword } from "../../../store/actions/login.action";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import history from "../../../history";
import Message from "../../Message/Message";
import {
  errorMessages,
  confirmationMessages,
} from "../../../constants/Constants";
import logo from "../../Login/logo.png";
import "./forgotPassword.css";
import Loader from "../../Loader/Loader"


class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      showMessage: false,
      showLoader: false,
      messageData: {},
      password: "",
      serverError: "",
      error: {
        userNameError: "",
        passwordError: "",
      },
    };
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  closeShowMessage = () => {
    this.setState({ showMessage: false });
  };
  validate = () => {
    let isError = false;
    let tempError = this.state.error;
    if (!this.state.userName) {
      isError = true;
      tempError.userNameError = errorMessages.EMAIL_ERROR;
    } else if (
      this.state.userName &&
      !this.state.userName.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    ) {
      isError = true;
      tempError.userNameError = errorMessages.INVALID_EMAIL;
    } else {
      tempError.userNameError = "";
    }
    if (isError) {
      this.setState({
        error: tempError,
      });
    }
    return isError;
  };

  changeToLogin = () => {
    history.push("/login");
  };

  onForgotPassword = (e) => {
    e.preventDefault();
    var valid = this.validate();
    if (!valid) {
      this.setState({
        showLoader : true
      })
      var userData = {
        emailId: this.state.userName,
      };
      this.props.forgotPassword(userData);
    }
    // history.push("/Dashboard")
  };

  async componentDidUpdate(prevProps) {
    const { forgotPasswordInfo } = this.props;
    if (prevProps.forgotPasswordInfo !== forgotPasswordInfo) {
      console.log(forgotPasswordInfo.response);
      if (
        forgotPasswordInfo.status === 200 ||
        forgotPasswordInfo.status === 201
      ) {
        this.setState({
          showMessage: true,
          showLoader:false,
          messageData: {
            type: "success",
            buttonValue: "Submit",
            message:
              confirmationMessages.PASSWORD_LINK_SEND + this.state.userName,
          },
        });
      } else if (
        forgotPasswordInfo &&
        forgotPasswordInfo.response &&
        forgotPasswordInfo.response.status === 401
      ) {
        if (forgotPasswordInfo.response.data) {
          this.setState({
            showLoader:false,
            serverError: forgotPasswordInfo.response.data,
          });
        }
      }
      else{
        this.setState({
          showLoader : false,
          serverError : "Something went wrong"
        })
      }
    }
  }
  render() {
    const { userName } = this.state;
    return (
      <Container fluid className="login-border">
        <div className="my-3 d-flex align-items-center justify-content-center">
            <img src={logo} alt="My logo" className="logo" />
          </div>
        <div className="d-flex align-items-center justify-content-center  login-wrapper">
         {/*  <div className="my-3">
            <img src={logo} alt="My logo" />
          </div> */}
          <form className="col-sm-10 col-lg-4 login-box">
            <div className="form-group">
              <h5 className="align-center">Forgot your Password?</h5>
              <label>
                Password reset link will be sent to the following email id
              </label>
              <input
                type="text"
                name="userName"
                value={userName}
                onChange={this.onChange}
                className={
                  "form-control" +
                  (this.state.error.userNameError ? " invalid" : " valid")
                }
                placeholder="Enter email"
              />
              <span className="errorMessage">
                {this.state.error.userNameError}
              </span>
              <span className="errorMessage">{this.state.serverError}</span>
            </div>
            <button
              type="submit"
              onClick={this.onForgotPassword}
              className="btn btn-primary btn-block login"
            >
              Submit
            </button>
            <p
              className="forgot-password forgot-password-text mt-3"
              onClick={this.changeToLogin}
            >
              Back to Login page
            </p>
          </form>
        </div>
        {this.state.showMessage ? (
          <Message
            msgIsOpen={this.state.showMessage}
            msgIsClose={this.closeShowMessage}
            messageData={this.state.messageData}
            buttonValue="Ok"
            hideCancel={true}
            action={this.changeToLogin}
          ></Message>
        ) : (
          ""
        )}
          { this.state.showLoader ? 
        <Loader/>:<div></div>}
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      forgotPassword: forgotPassword,
    },
    dispatch
  );
}
function mapStateToProps({ loginReducer }) {
  return {
    forgotPasswordInfo: loginReducer.forgotPassword,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)
);
