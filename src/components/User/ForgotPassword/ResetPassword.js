import React, { Component } from "react";
import "../../Login/Login.scss";
import { Container } from "react-bootstrap";
import { setNewPassword } from "../../../store/actions/login.action";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import history from "../../../history";
import Message from "../../Message/Message";
import {
  errorMessages,
  confirmationMessages,
} from "../../../constants/Constants";
import { toast } from "react-toastify";
import logo from "../../Login/logo.png";
import "./forgotPassword.css";
import Loader from "../../Loader/Loader"

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      showMessage: false,
      messageData: {},
      showLoader:false,
      password: "",
      emailId: "",
      newPassword: "",
      confirmNewPassword: "",
      serverError: "",
      error: {
        emailIdError: "",
        newPasswordError: "",
        confirmNewPasswordError: "",
      },
    };
  }
  changeToLogin = () => {
    history.push("/login");
  };

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  closeShowMessage = () => {
    this.setState({ showMessage: false });
  };
  validate = () => {
    let isError = false;
    let tempError = this.state.error;
    if (!this.state.newPassword) {
      isError = true;
      tempError.newPasswordError = errorMessages.PASSWORD_ERROR;
    } else if (this.state.newPassword && this.state.newPassword.length < 6) {
      tempError.newPasswordError = errorMessages.PASSWORD_LENGTH_ERROR;
    } else {
      tempError.newPasswordError = "";
    }

    if (!this.state.emailId) {
      isError = true;
      tempError.emailIdError = errorMessages.EMAIL_ERROR;
    } else if (
      this.state.emailId &&
      !this.state.emailId.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    ) {
      isError = true;
      tempError.emailIdError = errorMessages.INVALID_EMAIL;
    } else {
      tempError.emailIdError = "";
    }

    if (!this.state.confirmNewPassword) {
      isError = true;
      tempError.confirmNewPasswordError = errorMessages.CONFRIM_PASSWORD_ERROR;
    } else {
      tempError.confirmNewPasswordError = "";
    }
    if (this.state.newPassword && this.state.confirmNewPassword) {
      if (this.state.newPassword !== this.state.confirmNewPassword) {
        isError = true;
        tempError.confirmNewPasswordError =
          errorMessages.MATCH_CONFIRM_PASSWORD;
      }
    }
    if (isError) {
      this.setState({
        error: tempError,
      });
    }
    return isError;
  };
  onResetPassword = (e) => {
    e.preventDefault();
    var valid = this.validate();
    if (!valid) {
      this.setState({
        showLoader:true
      })
      var userData = {
        emailId: this.state.emailId,
        password: this.state.newPassword,
        authToken: this.props.match.params.authToken,
      };
      this.props.setNewPassword(userData);
    }
    // history.push("/Dashboard")
  };

  componentDidMount() {
    console.log("Auth Token", this.props.match.params.authToken);
  }
  async componentDidUpdate(prevProps) {
    const { setNewPasswordInfo } = this.props;
    if (prevProps.setNewPasswordInfo !== setNewPasswordInfo) {
      if (
        setNewPasswordInfo.status === 200 ||
        setNewPasswordInfo.status === 201
      ) {
        this.setState({
          showLoader : false
        })
        this.changeToLogin();
        toast.success(confirmationMessages.PASSWORD_CHANGE_SUCCESS, {
          position: toast.POSITION.BOTTOM_RIGHT,
        });
      } else if (
        setNewPasswordInfo &&
        setNewPasswordInfo.response &&
        setNewPasswordInfo.response.status === 401
      ) {
        if (setNewPasswordInfo.response.data) {
           this.setState({
            showLoader : false,
            serverError: setNewPasswordInfo.response.data,
          });
        }
      }
      else{
        this.setState({
          serverError:"Something went wrong",
          showLoader : false
        })
      }
    }
  }
  render() {
    const { emailId, newPassword, confirmNewPassword } = this.state;
    return (
      <Container fluid className="login-border">
        <div className="my-3 d-flex align-items-center justify-content-center">
            <img src={logo} alt="My logo" className="logo" />
          </div>
        <div className="d-flex align-items-center justify-content-center  login-wrapper">
         {/*  <div className="my-3">
            <img src={logo} alt="My logo" />
          </div> */}
          <form className="col-sm-10 col-lg-4 login-box">
            <h5 className="align-center">Reset Password</h5>
            <div className="form-group">
              <label>Email </label>
              <input
                type="text"
                name="emailId"
                value={emailId}
                onChange={this.onChange}
                className={
                  "form-control" +
                  (this.state.error.emailIdError ? " invalid" : " valid")
                }
                placeholder="Enter email"
              />
              <span className="errorMessage">
                {this.state.error.emailIdError}
              </span>
            </div>

            <div className="form-group">
              <label>New Password</label>
              <input
                type="password"
                name="newPassword"
                value={newPassword}
                onChange={this.onChange}
                className={
                  "form-control" +
                  (this.state.error.newPasswordError ? " invalid" : " valid")
                }
                placeholder="Enter new password"
              />
              <span className="errorMessage">
                {this.state.error.newPasswordError}
              </span>
            </div>

            <div className="form-group">
              <label> Confirm New Password</label>
              <input
                type="password"
                name="confirmNewPassword"
                value={confirmNewPassword}
                onChange={this.onChange}
                className={
                  "form-control" +
                  (this.state.error.confirmNewPasswordError
                    ? " invalid"
                    : " valid")
                }
                placeholder="Confirm new password"
              />
              <span className="errorMessage">
                {this.state.error.confirmNewPasswordError}
              </span>
              <span className="errorMessage">{this.state.serverError}</span>
            </div>

            <button
              type="submit"
              onClick={this.onResetPassword}
              className="btn btn-primary btn-block login"
            >
              Submit
            </button>
            <p
              className="forgot-password forgot-password-text mt-3"
              onClick={this.changeToLogin}
            >
              Back to Login page
            </p>
          </form>
        </div>
        {this.state.showMessage ? (
          <Message
            msgIsOpen={this.state.showMessage}
            msgIsClose={this.closeShowMessage}
            messageData={this.state.messageData}
            buttonValue="Go back to Login page"
            hideCancel={true}
            action={this.changeToLogin}
          ></Message>
        ) : (
          ""
        )}
          { this.state.showLoader ? 
        <Loader/>:<div></div>}
      </Container>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setNewPassword: setNewPassword,
    },
    dispatch
  );
}
function mapStateToProps({ loginReducer }) {
  return {
    setNewPasswordInfo: loginReducer.setNewPassword,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)
);
