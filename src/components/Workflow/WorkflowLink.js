import React, { Component } from "react";
import { Button, Input } from "reactstrap";
import { Modal } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Message from "../Message/Message";
import {
  getAllDraftWorkflows,
  getAllDraftWorkflowById,
  sendLinkToMail,
} from "../../store/actions/workflow.action";
import "./Workflow.scss";
import { toast } from "react-toastify";
import { confirmationMessages } from "../../constants/Constants";
import Loader from "../Loader/Loader"

class WorkflowLink extends Component {
  constructor(props) {
    super(props);
    this.state = {
      digramInstant: {},
      emailId: "",
      showMessage: false,
      showLoader:false,
      error: {
        emailIdError: "",
      },
    };
  }

  componentDidMount() {
    console.log("--", this.props.workflowId);
  }

  closeShowMessage = () => {
    this.setState({ showMessage: false });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.workflowId !== this.props.workflowId) {
      console.log(this.props.workflowId);
    }
    if (prevProps.exportToMailData !== this.props.exportToMailData) {
      if(this.props.exportToMailData.status === 200 || this.props.exportToMailData.status  === 201)
       {
      this.props.onHide();
      this.setState({
        showLoader:false
      })
      toast.success(confirmationMessages.EXPORT_SUCCESS + this.state.emailId, {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
      this.setState({
        emailId :""
      })
   }
  else{
    this.setState({
      showLoader:false
    })
    this.props.onHide();
    toast.error(confirmationMessages.EXPORT_ERROR + this.state.emailId, {
      position: toast.POSITION.BOTTOM_RIGHT,
    });
  } 
  }
}
  onClickView = (e) => {
    const win = window.open("/ExportWorkflow/" + this.props.workflowId);
    win.focus();
  };
  OnPropertyChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  validate = () => {
    let isError = false;
    let tempError = this.state.error;
    if (
      !this.state.emailId ||
      (this.state.emailId && !this.state.emailId.replace(/\s/g, "").length)
    ) {
      isError = true;
      tempError.emailIdError = "Email id required";
    } else {
      tempError.emailIdError = "";
    }

    if (isError) {
      this.setState({
        error: tempError,
      });
    }
    return isError;
  };

  sendEmail = () => {
    const validateError = this.validate();
    if (!validateError) {
      this.setState({
        showLoader: true
      })
      var emailData = {
        emailId: this.state.emailId,
        workflowId: this.props.workflowId,
      };
      this.props.sendLinkToMail(emailData);
    }
  };
  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.onHide}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Get Workflow Link
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <label>Enter Recipients Email ID</label>
          <Input
            type="text"
            autoComplete="off"
            placeholder="Email"
            name="emailId"
            value={this.state.emailId}
            className={
              "inputerror " +
              (this.state.error.emailIdError ? "invalid" : "valid")
            }
            onChange={this.OnPropertyChange}
          ></Input>
          <span className="errorMessage">{this.state.error.emailIdError}</span>
          {this.state.showLoader ? 
        <Loader/>:<div></div>}
        </Modal.Body>
        <Modal.Footer>
         
          <Button size="sm" color="primary" onClick={this.sendEmail}>
            Send
          </Button>
        </Modal.Footer>
        {this.state.showMessage ? (
          <Message
            msgIsOpen={this.state.showMessage}
            msgIsClose={this.closeShowMessage}
            messageData={this.state.messageData}
            buttonValue="Delete"
            action={(event) => this.deleteWorkflow(event)}
          ></Message>
        ) : (
          ""
        )}
      </Modal>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getAllDraftWorkflows: getAllDraftWorkflows,
      getAllDraftWorkflowById: getAllDraftWorkflowById,
      sendLinkToMail: sendLinkToMail,
    },
    dispatch
  );
}

function mapStateToProps({ workflowReducer }) {
  return {
    getWorkflowDraftByIdData: workflowReducer.getWorkflowDraftById,
    exportToMailData: workflowReducer.exportToMail,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(WorkflowLink)
);
