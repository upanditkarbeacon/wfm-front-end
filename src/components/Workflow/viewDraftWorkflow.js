import React, { Component } from "react";
import { Button, Input } from "reactstrap";
import { Modal } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Message from "../Message/Message";
import DataTable from "react-data-table-component";
import {
  getAllDraftWorkflows,
  getAllDraftWorkflowById,
  deleteWorkflow,
} from "../../store/actions/workflow.action";
import "./Workflow.scss";
import { toast } from "react-toastify";
import { confirmationMessages } from "../../constants/Constants";

class ViewDraftWorkflow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      digramInstant: {},
      showMessage: false,
      messageData: {},
      sourceSearchText: "",
    };
    this.load = this.load.bind(this);
  }

  componentDidMount() {
    this.props.getAllDraftWorkflows();
  }
  onSearchChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
  closeShowMessage = () => {
    this.setState({ showMessage: false });
  };
  async load(e) {
    await this.props.getDiagram(JSON.parse(e));
    await this.props.getDiagram(JSON.parse(e));
  }

  deleteWorkflow(e) {
    this.props.deleteWorkflow(e.workflowId);
  }

  onClickDelete(e) {
    console.log(e.target.value);
    this.setState({
      showMessage: true,
      messageData: {
        type: "success",
        message: confirmationMessages.DELETE_WORKFLOW,
        workflowId: e.target.value,
      },
    });
  }
  componentDidUpdate(prevProps) {
    if (prevProps.deleteWorkflowData !== this.props.deleteWorkflowData) {
      console.log(this.props.deleteWorkflowData);
      if (
        this.props.deleteWorkflowData.status === 200 ||
        this.props.deleteWorkflowData.status === 201
      ) {
        this.closeShowMessage();
        this.props.getAllDraftWorkflows();
        toast.success(confirmationMessages.DELETE_SUCCESS, {
          position: toast.POSITION.BOTTOM_RIGHT,
        });
      }
    }
    if (
      prevProps.getWorkflowDraftByIdData !== this.props.getWorkflowDraftByIdData
    ) {
      this.load(this.props.getWorkflowDraftByIdData.data.workflowJson);
      this.props.workflowName(
        this.props.getWorkflowDraftByIdData.data.workflowName
      );
      this.props.workflowId(
        this.props.getWorkflowDraftByIdData.data.workflowId
      );
      this.props.onHide();
    }
  }
  onClickView(e) {
    this.props.getAllDraftWorkflowById(e.target.value);
  }

  render() {
    var data = [];
    const { sourceSearchText } = this.state;
    if (
      this.props.getAllWorkflowDraftData &&
      this.props.getAllWorkflowDraftData.data
    ) {
      var getAllWorkflowDraftData = this.props.getAllWorkflowDraftData.data.filter(
        function (item) {
          return (
            JSON.stringify(item.workflowName)
              .toLowerCase()
              .indexOf(sourceSearchText.toLowerCase()) !== -1
          );
        }
      );
    }

    if (getAllWorkflowDraftData) {
      var obj = Object.keys(getAllWorkflowDraftData);
      for (var i = 0; i < obj.length; i++) {
        data.push({
          srNo: i + 1,
          workflowName: getAllWorkflowDraftData[i].workflowName,
          actions: (
            <div className="crud-btn">
              <Button
                color="Secondary"
                value={getAllWorkflowDraftData[i].workflowId}
                className="button-menu lni-eye"
                onClick={(e) => this.onClickView(e, "value")}
                title="View Workflow"
              ></Button>
              <Button
                color="Secondary"
                value={getAllWorkflowDraftData[i].workflowId}
                className="button-menu lni-trash"
                onClick={(e) => this.onClickDelete(e, "value")}
                title="Delete Workflow"
              ></Button>
            </div>
          ),
        });
      }
    }
    const columns = [
      {
        name: "Sr.No",
        selector: "srNo",
        sortable: true,
        width: "200px",
      },
      {
        name: "Workflow Name",
        selector: "workflowName",
        sortable: true,
      },
      {
        name: "Actions",
        selector: "actions",
        sortable: false,
      },
    ];

    return (
      <Modal
        show={this.props.show}
        onHide={this.props.onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Saved Workflows
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="div-mapper-search">
            <Input
              type="text"
              autoComplete="off"
              placeholder="Search"
              value={sourceSearchText}
              name="sourceSearchText"
              onChange={this.onSearchChange}
            ></Input>
          </div>
          <DataTable
            className="Mytable"
            columns={columns}
            data={data}
            pagination
            paginationPerPage={5}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button size="sm" onClick={this.props.onHide}>
            Close
          </Button>
        </Modal.Footer>
        {this.state.showMessage ? (
          <Message
            msgIsOpen={this.state.showMessage}
            msgIsClose={this.closeShowMessage}
            messageData={this.state.messageData}
            buttonValue="Delete"
            action={(event) => this.deleteWorkflow(event)}
          ></Message>
        ) : (
          ""
        )}
      </Modal>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getAllDraftWorkflows: getAllDraftWorkflows,
      getAllDraftWorkflowById: getAllDraftWorkflowById,
      deleteWorkflow: deleteWorkflow,
    },
    dispatch
  );
}

function mapStateToProps({ workflowReducer }) {
  return {
    getAllWorkflowDraftData: workflowReducer.getAllWorkflowDraft,
    getWorkflowDraftByIdData: workflowReducer.getWorkflowDraftById,
    deleteWorkflowData: workflowReducer.deleteWorkflow,
  };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewDraftWorkflow)
);
