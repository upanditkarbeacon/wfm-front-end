export const errorMessages={
    NO_ERROR:"No Error found",
    WORKFLOW_PUBLISH_FAIL:"Failed to publish workflow",
    WORKFLOW_SAVE_FAIL:"Failed to save workflow",
    DUPLICATE_WORKFLOW:" already exist",
    WARNING_LOOP_DETETCTED:"Loop detected in the workflow.Click Continue to ignore loops or Fix Loop",
    WORKFLOW_NAME: "Workflow name is required",
    INVALID_NODE_MESSAGE : "Please fill all mandatory properties",
    INVALID_LENGTH_MESSAGE : "Exceeded max character limit of ",
    CASE_TYPE_NOT_FOUNT  :"Error : Case Type node not found in workflow",
    INVALID_NODE_AFTER_CASE_TYPE:"Only Task node is allowed after Case Type node",
    INVALID_NODE_AFTER_DISPOSITION_OUTCOME:"There should be Task node after Disposition Outcome",
    INVALID_NODE_AFTER_TASK:"There should be Outcome node after Task node",
    DUPLICATE_OUTCOME_NODE:"This task node contains duplicate outcome nodes",
    INVALID_NUMBER_OF_CASE_TYPE:"Case Type node allowed only once in the workflow",
    INVALID_CASE_TYPE_NODE_POSITION:"Case type node allowed only at start of the workflow",
    EMPTY_WORKFLOW:"Error : Workflow is empty",
    BROKEN_LINK : "Please link this node",
    INVALID_OUTCOME_NODE_POSITION:"Outcome Node should not be at the start of workflow",
    OLD_PASSWORD_ERROR:"Old password is required",
    NEW_PASSWORD_ERROR:"New password is required",
    PASSWORD_ERROR:"Password is required",
    PASSWORD_LENGTH_ERROR:"Password should be atleast 6 characters long",
    CONFRIM_PASSWORD_ERROR:"Confirm password is required",
    MATCH_CONFIRM_PASSWORD:"Confirm password should match with new password",
    EMAIL_ERROR:"Email Id is required",
    INVALID_EMAIL:"Invalid email"
}

export const confirmationMessages={
    WORKFLOW_TEXT:"Workflow ",
    SAVED_SUCCESS:" Saved successfully",
    PUBLISHED_SUCCESS:"Published Successfully",
    SAVE_WORKFLOW:"Are you sure you want to save this workflow?",
    DELETE_SUCCESS:"Workflow deleted successfully",
    EXPORT_SUCCESS:"Workflow Link has been sent to ",
    EXPORT_ERROR: "Error in sending mail to ",
    PUBLISH_WORKFLOW: "Are you sure you want to publish this workflow?",
    DELETE_WORKFLOW:"Are you sure you want to delete this workflow?",
    PASSWORD_CHANGE_SUCCESS:"Password has changed successfully",
    PASSWORD_LINK_SEND:"Password reset link has been sent to",
    
}

export const maxLength={
    OUTCOME_MAX_LENGTH :" 50 ",
    TASK_MAX_LENGTH :"50",
    ACTIVITY_MAX_LENGTH:"50",
    ACTIVITY_TYPE_MAX_LENGTH:"50",
    DESCRIPTION_MAX_LENGTH:"256",
    TRIGGER_OUTCOME_MAX_LENGTH:"50",
    CASE_TYPE_MAX_LENGTH:"50",
    CASE_CATEGORY_MAX_LENGTH:"50",
    REMARK_MAX_LENGTH:"50",
    REVIEW_TYPE_MAX_LENGTH:"50",
    DISPOSITION_OUTCOME_LENGTH:"50"
}