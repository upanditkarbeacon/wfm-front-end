import axios from "axios";
import "es6-promise/auto";

let headers;



export const loginService = (url, data) => {
  return new Promise((resolve) => {
      axios({
          method: "post",
          url: url,
          data: data
      })
          .then(response => {
              if (response) {
                  resolve(response);
              }
          })
          .catch(error => {
              if (error) {
                  resolve(error);
              }
          });
  });
};
export const ssoLoginService = (url, data) => {
  return new Promise((resolve) => {
      axios({
          method: "post",
          url: url,
          data: data
      })
          .then(response => {
              if (response) {
                  resolve(response);
              }
          })
          .catch(error => {
              if (error) {
                  resolve(error);
              }
          });
  });
};

export const getData = (url) => {
  headers = {
    "content-type": "application/json",
  };

  return new Promise((resolve) => {
    axios({
      method: "get",
      url: url,
      headers: headers,
    })
      .then((response) => {
        if (response) {
          resolve(response);
        }
      })
      .catch((error) => {
        if (error) {
          if (error.response) {
            if (error.response.status === 500) {
            }
          }
          resolve(error);
        }
      });
  });
};

export const postData = (url, data) => {
  headers = {
    "content-type": "application/json",
  };
  return new Promise((resolve) => {
    axios({
      method: "post",
      url: url,
      headers: headers,
      data: data,
    })
      .then((response) => {
        if (response) {
          resolve(response);
        }
      })
      .catch((error) => {
        if (error) {
          if (error.response) {
            if (error.response.status === 500) {
            }
          }
          resolve(error);
        }
      });
  });
};


export const deleteData = (url) => {
      headers = {
          'content-type': 'application/json',
      }
      return new Promise((resolve) => {
          axios({
              method: "delete",
              url: url,
              headers: headers,
          })
              .then(response => {
                  if (response) {
                      resolve(response);
                  }
              })
              .catch(error => {
                  if (error) {
                      if (error.response) {
                          if (error.response.status === 500) {
                             
                          }
                      }
                      resolve(error);
                  }
              });
      });
  }


