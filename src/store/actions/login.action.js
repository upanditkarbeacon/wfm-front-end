import { APP_URL } from "../../config.js";
import { loginService, postData } from "./../../services/service";
export const LOGIN = "LOGIN";
export const FORGOT_PASSWORD = "FORGOT_PASSWORD";
export const SET_NEW_PASSWORD = "SET_NEW_PASSWORD";
export const CHANGE_PASSWORD = "CHANGE_PASSWORD";

export function login(loginData) {
  return (dispatch) =>
    loginService(APP_URL + "WFA/authenticate", loginData).then((response) => {
      if (response) {
        return dispatch({
          type: LOGIN,
          payload: response,
        });
      }
    });
}
export function forgotPassword(loginData) {
  return (dispatch) =>
    loginService(APP_URL + "WFA/forgotPassword", loginData).then((response) => {
      if (response) {
        return dispatch({
          type: FORGOT_PASSWORD,
          payload: response,
        });
      }
    });
}

export function setNewPassword(loginData) {
  return (dispatch) =>
    loginService(APP_URL + "WFA/setNewPassword", loginData).then((response) => {
      if (response) {
        return dispatch({
          type: SET_NEW_PASSWORD,
          payload: response,
        });
      }
    });
}

export function changePassword(data) {
  return (dispatch) =>
    postData(APP_URL + "WFA/resetPassword", data).then((response) => {
      if (response) {
        return dispatch({
          type: CHANGE_PASSWORD,
          payload: response,
        });
      }
    });
}