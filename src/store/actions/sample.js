
import { getData } from './../../services/service';
import { APP_URL } from "./../../config.js";

export const GET_CASE_TYPE = "GET_CASE_TYPE";
export const NODE_DATA_ARRAY= "NODE_DATA_ARRAY"

export function getCaseTypes() {
    return {
      type: GET_CASE_TYPE,
      payload: [
        { id: 1, case_type: "Part D Pre Service Appeals" },
        { id: 2, case_type: "Part C Pre Service Appeals" },
      ],
}
}


export function getAllNodes() {
  return dispatch =>
      getData(APP_URL + "WFA/data/nodes").then(response => {
          if (response) {
              return dispatch({
                  type: NODE_DATA_ARRAY,
                  payload: response
              })
          }

      })
}

