import { postData } from '../../services/service';
import { APP_URL } from "../../config.js";
import { getData } from './../../services/service';

export const SAVE_WORKFLOW_AS_DRAFT = "SAVE_WORKFLOW_AS_DRAFT"
export const GET_DRAFT_WORKFLOWS = "GET_DRAFT_WORKFLOWS"
export const GET_DRAFT_WORKFLOWS_BY_ID= "GET_DRAFT_WORKFLOWS_BY_ID"
export const DELETE_WORKFLOW ="DELETE_WORKFLOW"
export const GET_PREDEFINED_VALUES = "GET_PREDEFINED_VALUES"
export const SAVE_PREDEFINED_LIST = "SAVE_PREDEFINED_LIST"
export const PUBLISH_WOKFLOW = "PUBLISH_WORKFLOW"
export const DETECT_CYCLE = "DETECT_CYCLE"
export const IMPACTED_LIST ="IMPACTED_LIST"
export const EXPORT_TO_MAIL = "EXPORT_TO_MAIL"
export const GET_EXPORTED_WORKFLOW = "GET_EXPORTED_WORKFLOW"

export function saveWorkflowAsDraft(workflowData) {
    return dispatch =>
        postData(APP_URL + "WFA/workflow/saveWorkflow", workflowData).then(response => {
            if (response) {
                return dispatch({
                    type: SAVE_WORKFLOW_AS_DRAFT,
                    payload: response
                });
            }
        })
}
export function publishWorkflow(workflowId,workflowData) {
    return dispatch =>
        postData(APP_URL + "WFA/workflow/publishWorkflow/"+workflowId, workflowData).then(response => {
            if (response) {
                return dispatch({
                    type: PUBLISH_WOKFLOW,
                    payload: response
                });
            }
        })
}

export function getAllDraftWorkflows(){
    return dispatch =>
    getData(APP_URL + "WFA/workflow/savedWorkflows/").then(response => {
        if (response) {
            return dispatch({
                type: GET_DRAFT_WORKFLOWS,
                payload: response
            })
        }

    })
}

export function getAllDraftWorkflowById(workflowId){
    return dispatch =>
    getData(APP_URL + "WFA/workflow/savedWorkflows/"+workflowId).then(response => {
        if (response) {
            return dispatch({
                type: GET_DRAFT_WORKFLOWS_BY_ID,
                payload: response
            })
        }

    })
}

export function getExportedWorkflowById(workflowId){
    return dispatch =>
    getData(APP_URL + "WFA/workflow/exportWorkflow/"+workflowId).then(response => {
        if (response) {
            return dispatch({
                type: GET_EXPORTED_WORKFLOW,
                payload: response
            })
        }

    })
}

export function deleteWorkflow(workflowId){
    return dispatch =>
    postData(APP_URL + "WFA/workflow/deleteWorkflow",workflowId).then(response => {
        if (response) {
            return dispatch({
                type: DELETE_WORKFLOW,
                payload: response
            })
        }

    })
}

export function getPredefinedValues(type){
    return dispatch =>
    getData(APP_URL + "WFA/data/configuredValues/"+type).then(response => {
        if (response) {
            return dispatch({
                type: GET_PREDEFINED_VALUES,
                payload: response
            })
        }

    })
}
    
export function saveToPredefinedValueList(configId,configuration){
    return dispatch =>
    postData(APP_URL + "WFA/data/configuredValues/"+configId,configuration).then(response => {
        if (response) {
            return dispatch({
                type: SAVE_PREDEFINED_LIST,
                payload: response
            })
        }

    })

}

export function detectCycle(workflowData) {
    return dispatch =>
        postData( APP_URL+"WFA/workflow/detectCycle", workflowData).then(response => {
            if (response) {
                return dispatch({
                    type: DETECT_CYCLE,
                    payload: response
                });
            }
        })
}

export function checkImpactedList(caseCategory) {
    return dispatch =>
        postData(APP_URL + "WFA/workflow/impactedCases/", caseCategory).then(response => {
            if (response) {
                return dispatch({
                    type: IMPACTED_LIST,
                    payload: response
                });
            }
        })
}

export function sendLinkToMail(data) {
    return dispatch =>
        postData(APP_URL + "WFA/workflow/exportWorkflow/", data).then(response => {
            if (response) {
                return dispatch({
                    type: EXPORT_TO_MAIL,
                    payload: response
                });
            }
        })
}