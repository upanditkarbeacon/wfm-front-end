import { combineReducers } from "redux";

import workflowReducer from "./workflowReducer"
import loginReducer from "./loginReducer"
const appReducer = combineReducers({workflowReducer,loginReducer});

export default appReducer;
