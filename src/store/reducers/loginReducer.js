import {
  LOGIN,
  FORGOT_PASSWORD,
  SET_NEW_PASSWORD,
  CHANGE_PASSWORD
} from "../actions/login.action";

const initialState = {
  loginData: "",
  forgotPassword: "",
  setNewPassword: "",
  changePassword: ""
};

const loginReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        loginData: action.payload,
      };
    }
    case CHANGE_PASSWORD: {
      return {
        ...state,
        changePassword: action.payload,
      };
    }
    case SET_NEW_PASSWORD: {
      return {
        ...state,
        setNewPassword: action.payload,
      };
    }
    case FORGOT_PASSWORD: {
      return {
        ...state,
        forgotPassword: action.payload,
      };
    }
    default: {
      return {
        state,
      };
    }
  }
};
export default loginReducer;
