import { GET_CASE_TYPE, NODE_DATA_ARRAY } from "../actions/sample";
import {
  DETECT_CYCLE,
  SAVE_WORKFLOW_AS_DRAFT,
  GET_DRAFT_WORKFLOWS,
  GET_DRAFT_WORKFLOWS_BY_ID,
  DELETE_WORKFLOW,
  GET_PREDEFINED_VALUES,
  SAVE_PREDEFINED_LIST,
  PUBLISH_WOKFLOW,
  IMPACTED_LIST,
  EXPORT_TO_MAIL,
  GET_EXPORTED_WORKFLOW
} from "../actions/workflow.action";

const initialState = {
  caseTypeData: "",
  nodeDataArray: "",
  saveWorkflowAsDraft: "",
  getAllWorkflowDraft: "",
  getWorkflowDraftById: "",
  deleteWorkflow: "",
  getPredefinedValues: "",
  saveToPredefinedList:"",
  publishedWorkflow:"",
  detectCycleInWorkflow:"",
  impactedList:"",
  exportToMail:"",
  exportedWorkflow:""
};

const workflowReducer = function (state = initialState, action) {
  switch (action.type) {
    case SAVE_WORKFLOW_AS_DRAFT: {
      return {
        ...state,
        saveWorkflowAsDraft: action.payload,
      };
    }
    case DETECT_CYCLE: {
      return {
        ...state,
        detectCycleInWorkflow: action.payload,
      };
    }
    case IMPACTED_LIST: {
      return {
        ...state,
        impactedList: action.payload,
      };
    }
    case GET_EXPORTED_WORKFLOW:{
      return{
      ...state,
      exportedWorkflow: action.payload
    }
  }
    case EXPORT_TO_MAIL: {
      return {
        ...state,
        exportToMail: action.payload,
      };
    }
    case PUBLISH_WOKFLOW: {
      return {
        ...state,
        publishedWorkflow: action.payload,
      };
    }
    case SAVE_PREDEFINED_LIST: {
      return {
        ...state,
        saveToPredefinedList: action.payload,
      };
    }
    case GET_PREDEFINED_VALUES: {
      return {
        ...state,
        getPredefinedValues: action.payload,
      };
    }
    case GET_DRAFT_WORKFLOWS: {
      return {
        ...state,
        getAllWorkflowDraft: action.payload,
      };
    }
    case DELETE_WORKFLOW: {
      return {
        ...state,
        deleteWorkflow: action.payload,
      };
    }
    case GET_DRAFT_WORKFLOWS_BY_ID: {
      return {
        ...state,
        getWorkflowDraftById: action.payload,
      };
    }
    case GET_CASE_TYPE: {
      return {
        ...state,
        caseTypeData: action.payload,
      };
    }
    case NODE_DATA_ARRAY: {
      return {
        ...state,
        nodeDataArray: action.payload,
      };
    }
    default: {
      return {
        state,
      };
    }
  }
};
export default workflowReducer;
