import * as go from "gojs";

export function contextMenuList(diagram, $, data) {


  diagram.contextMenu = $(
    "ContextMenu",
/*     $(
      "ContextMenuButton",
      $(go.TextBlock, "Undo"),

      {
        click: function (e, obj) {
          e.diagram.commandHandler.undo();
        },
      },
      new go.Binding("visible", "", function (o) {
        return o.diagram.commandHandler.canUndo();
      }).ofObject()
    ), */
    $(
      "ContextMenuButton",
      $(go.TextBlock, "Redo"),
      {
        click: function (e, obj) {
          e.diagram.commandHandler.redo();
        },
      },
      new go.Binding("visible", "", function (o) {
        return o.diagram.commandHandler.canRedo();
      }).ofObject()
    ),
    // no binding, always visible button:
    $("ContextMenuButton", $(go.TextBlock, "Task"), {
      click: function (e, obj) {
        e.diagram.commit(function (d) {
          var datanode = returnNode(data, "Task");
          d.model.addNodeData(datanode);
          d.model.setDataProperty(datanode, "text", "");
          d.model.setDataProperty(datanode, "Task", "Task");
        });
      },
    }),
    $("ContextMenuButton", $(go.TextBlock, "Case Type"), {
      click: function (e, obj) {
        e.diagram.commit(function (d) {
          var datanode = returnNode(data, "Case Type");
          d.model.addNodeData(datanode);
          d.model.setDataProperty(datanode, "text", "");
          d.model.setDataProperty(datanode, "Case Type", "Case Type");
        });
      },
    }),

    $("ContextMenuButton", $(go.TextBlock, "Outcome"), {
      click: function (e, obj) {
        e.diagram.commit(function (d) {
          var datanode = returnNode(data, "Outcome");
          d.model.addNodeData(datanode);
          d.model.setDataProperty(datanode, "text", "");
          d.model.setDataProperty(datanode, "Outcome", "Outcome");
        });
      },
    }),
    $("ContextMenuButton", $(go.TextBlock, "Disposition Outcome"), {
      click: function (e, obj) {
        e.diagram.commit(function (d) {
          var datanode = returnNode(data, "Disposition Outcome");
          d.model.addNodeData(datanode);
          d.model.setDataProperty(datanode, "text", "");
          d.model.setDataProperty(datanode, "Disposition Outcome", "Disposition Outcome");
        });
      },
    }),
    $("ContextMenuButton", $(go.TextBlock, "Note"), {
      click: function (e, obj) {
        e.diagram.commit(function (d) {
          var datanode = returnNode(data, "Note");
          d.model.addNodeData(datanode);
          d.model.setDataProperty(datanode, "text", "");
          d.model.setDataProperty(datanode, "Note", "Note");
        });
      },
    }),
    $("ContextMenuButton", $(go.TextBlock, "Step"), {
      click: function (e, obj) {
        e.diagram.commit(function (d) {
          var datanode = returnNode(data, "step");
          d.model.addNodeData(datanode);
          d.model.setDataProperty(datanode, "text", "");
          d.model.setDataProperty(datanode, "step", "Step");
        });
      },
    })
  );
}

function returnNode(data, category) {
  for (var i = 0; i < data.length; i++) {
    if (category === data[i].category) {
      return data[i];
    }
  }
}
