import * as go from "gojs";

function getFont() {
  return "bold 10pt Arial";
}
function getFontNodeText() {
  return "10pt Arial";
}
function returnDiagram(diagram) {
  return diagram;
}

export function getNodes(diagram, $, data) {
  returnDiagram(diagram);
  var nodeResizeAdornmentTemplate = $(
    go.Adornment,
    "Spot",
    { locationSpot: go.Spot.Right },
    $(go.Placeholder),
    $(go.Shape, {
      alignment: go.Spot.TopLeft,
      cursor: "nw-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.Top,
      cursor: "n-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.TopRight,
      cursor: "ne-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.Left,
      cursor: "w-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.Right,
      cursor: "e-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.BottomLeft,
      cursor: "se-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.Bottom,
      cursor: "s-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    }),
    $(go.Shape, {
      alignment: go.Spot.BottomRight,
      cursor: "sw-resize",
      desiredSize: new go.Size(6, 6),
      fill: "lightblue",
      stroke: "deepskyblue",
    })
  );
  diagram.nodeTemplateMap.add(
    "Outcome",
    $(
      go.Node,

      "Vertical",
      {
        locationObjectName: "SHAPE",
        locationSpot: go.Spot.Center,
        selectionAdorned: false,
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      {
        resizable: true,
        resizeObjectName: "PANEL",
        resizeAdornmentTemplate: nodeResizeAdornmentTemplate,
      },
      new go.Binding("angle").makeTwoWay(),
      $(
        go.Panel,
        "Auto",
        { name: "PANEL" },
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(
          go.Size.stringify
        ),
        $(
          go.Shape,
          "Rectangle",
          {
            portId: "",
            fromLinkable: true,
            toLinkable: true,
            cursor: "pointer",
            minSize: new go.Size(80, 35),
            // maxSize: new go.Size(80,40),
            fill: "white",
            strokeWidth: 1,
            stroke: "white",
          },
          new go.Binding("figure"),
          new go.Binding("stroke", "Border Color"),
          new go.Binding("fill", "Background Color"),
          new go.Binding("stroke", "isHighlighted", function (h, color, check) {
            return h ? "rgb(255, 0, 0)" : "rgb(49, 112, 179)";
          }).ofObject(),
          new go.Binding("strokeWidth", "isHighlighted", function (h) {
            return h ? 2 : 1;
          }).ofObject() 
        ),
      
        $(
          go.TextBlock,
          {
            font: getFontNodeText(),
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
            stroke: "",
          },
          new go.Binding("text", "Outcome").makeTwoWay(),
          new go.Binding("stroke", "Font Color")
        )
      ),
      $(
        go.TextBlock,
        {
          font: getFont(),
          alignment: go.Spot.Center,
          textAlign: "center",
          margin: 2,
          "stroke":"black"
        },
        new go.Binding("text"),
        new go.Binding("text","errorText"),
        new go.Binding("stroke","stroke"),
      ),
      {
        contextMenu: getContextMenu("Outcome"), // define a context menu for each node
      }
    )
  );

  diagram.nodeTemplateMap.add(
    "step",
    $(
      go.Node,
      "Vertical",
      {
        locationObjectName: "SHAPE",
        locationSpot: go.Spot.Center,
        selectionAdorned: false,
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      {
        resizable: true,
        resizeObjectName: "PANEL",
        resizeAdornmentTemplate: nodeResizeAdornmentTemplate,
      },
      new go.Binding("angle").makeTwoWay(),
      $(
        go.Panel,
        "Auto",
        { name: "PANEL" },
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(
          go.Size.stringify
        ),
        $(
          go.Shape,
          "Rectangle",
          {
            portId: "",
            fromLinkable: true,
            toLinkable: true,
            cursor: "pointer",
            minSize: new go.Size(80, 10),
            // maxSize: new go.Size(80,40),
            fill: "white",
            strokeWidth: 1,
            stroke: "",
          },
          new go.Binding("figure"),
          new go.Binding("fill", "Background Color"),
          new go.Binding("stroke", "Border Color")
        ),
        $(
          go.TextBlock,
          {
            font: getFontNodeText(),
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
          },
          new go.Binding("text", "Step").makeTwoWay(),
          new go.Binding("stroke", "Font Color")
        )
      ),
      $(
        go.TextBlock,
        {
          font: getFont(),
          alignment: go.Spot.Center,
          textAlign: "center",
          margin: 2,
        },
        new go.Binding("text")
      ),
      {
        contextMenu: getContextMenu("Disposition Outcome"), // define a context menu for each node
      }
    )
  );

  diagram.nodeTemplateMap.add(
    "Disposition Outcome",
    $(
      go.Node,
      "Vertical",
      {
        locationObjectName: "SHAPE",
        locationSpot: go.Spot.Center,
        selectionAdorned: false,
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      {
        resizable: true,
        resizeObjectName: "PANEL",
        resizeAdornmentTemplate: nodeResizeAdornmentTemplate,
      },
      new go.Binding("angle").makeTwoWay(),
      $(
        go.Panel,
        "Auto",
        { name: "PANEL" },
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(
          go.Size.stringify
        ),
        $(
          go.Shape,
          "Rectangle",
          {
            portId: "",
            fromLinkable: true,
            toLinkable: true,
            minSize: new go.Size(75, 35),
            cursor: "pointer",
            // maxSize: new go.Size(80,40),
            fill: "white",
            strokeWidth: 1,
            stroke: "",
          },
          new go.Binding("figure"),
          new go.Binding("fill", "Background Color"),
          new go.Binding("stroke", "Border Color"),
          
          new go.Binding("stroke", "isHighlighted", function (h, type, check) {
            return h ? "rgb(255, 0, 0)" : "#1C9247";
          }).ofObject(),
          new go.Binding("strokeWidth", "isHighlighted", function (h) {
            return h ? 2 : 1;
          }).ofObject() 
        ),
        $(
          go.TextBlock,
          {
            font: getFontNodeText(),
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
          },
          new go.Binding("text", "Disposition Outcome").makeTwoWay(),
          new go.Binding("stroke", "Font Color")
        )
      ),
      $(
        go.TextBlock,
        {
          font: getFont(),
          alignment: go.Spot.Center,
          textAlign: "center",
          margin: 2,
        },
        new go.Binding("text"),
        new go.Binding("text","errorText"),
        new go.Binding("stroke","stroke"),
      ),
      {
        contextMenu: getContextMenu("Disposition Outcome"), // define a context menu for each node
      }
    )
  );

  diagram.nodeTemplateMap.add(
    "Task",
    $(
      go.Node,
      "Vertical",
      {
        locationObjectName: "SHAPE",
        locationSpot: go.Spot.Center,
        selectionAdorned: false,
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      {
        resizable: true,
        resizeObjectName: "PANEL",
        resizeAdornmentTemplate: nodeResizeAdornmentTemplate,
      },
      new go.Binding("angle").makeTwoWay(),
      $(
        go.Panel,
        "Auto",
        { name: "PANEL"},// margin: new go.Margin(50, 0, 0, 0) },
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(
          go.Size.stringify
        ),
        $(
          go.Shape,
          "Rectangle",
          {
            portId: "",
            fromLinkable: true,
            toLinkable: true,
            minSize: new go.Size(70, 37),
            cursor: "pointer",
            // maxSize: new go.Size(80,40),
            fill: "white",
            strokeWidth: 1,
            stroke: "",
          },
          new go.Binding("figure"),
          new go.Binding("fill", "Background Color"),
          new go.Binding("stroke", "Border Color"),
          new go.Binding("stroke", "isHighlighted", function (h, type, check) {
            return h ? "rgb(255, 0, 0)" : "#941b0c";
          }).ofObject(),
          new go.Binding("strokeWidth", "isHighlighted", function (h) {
            return h ? 2 : 1;
          }).ofObject() 
        
        ),
        $(
          go.TextBlock,
          {
            font: getFontNodeText(),
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
            stroke: "",
          },
          new go.Binding("text", "Task").makeTwoWay(),
          new go.Binding("stroke", "Font Color")
        )
      ),
      $(
        go.TextBlock,
        {
          font: getFont(),
          alignment: go.Spot.Center,
          textAlign: "center",
          margin: 2,
        },
        new go.Binding("text"),
        new go.Binding("text","errorText"),
        new go.Binding("stroke","stroke"),
      ),
      {
        contextMenu: getContextMenu("Task"), // define a context menu for each node
      }
    )
  );

  diagram.nodeTemplateMap.add(
    "Case Type",
    $(
      go.Node,

      "Vertical",
      {
        locationObjectName: "SHAPE",
        locationSpot: go.Spot.Center,
        selectionAdorned: false,
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      {
        resizable: true,
        resizeObjectName: "PANEL",
        resizeAdornmentTemplate: nodeResizeAdornmentTemplate,
      },
      new go.Binding("angle").makeTwoWay(),
      $(
        go.Panel,
        "Auto",
        { name: "PANEL" },
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(
          go.Size.stringify
        ),
        $(
          go.Shape,
          "Rectangle",
          {
            portId: "",
            fromLinkable: true,
            minSize: new go.Size(75, 10),
            toLinkable: true,
            cursor: "pointer",
            // maxSize: new go.Size(80,40),
            fill: "white",
            strokeWidth: 1,
            stroke: "",
          },
          new go.Binding("figure"),
          new go.Binding("fill", "Background Color"),
          new go.Binding("stroke", "Border Color"),
                 
           new go.Binding("stroke", "isHighlighted", function (h, type, check) {
            return h ? "rgb(255, 0, 0)" : "#cc5803";
          }).ofObject(),
          new go.Binding("strokeWidth", "isHighlighted", function (h) {
            return h ? 2 : 1;
          }).ofObject() 
        ),
        $(
          go.TextBlock,
          {
            font: getFontNodeText(),
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
            stroke: "",
          },
          new go.Binding("text", "Case Type").makeTwoWay(),
          new go.Binding("stroke", "Font Color")
        )
      ),
      $(
        go.TextBlock,
        {
         font: getFont(),
          alignment: go.Spot.Center,
          textAlign: "center",
          margin: 2,
        },
        new go.Binding("text"),
        new go.Binding("text","errorText"),
        new go.Binding("stroke","stroke"),

      ),
      {
        contextMenu: getContextMenu("Case Type"), // define a context menu for each node
      }
    )
  );

  diagram.nodeTemplateMap.add(
    "Note",
    $(
      go.Node,
      "Vertical",
      {
        locationObjectName: "SHAPE",
        locationSpot: go.Spot.Center,
        selectionAdorned: false,
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      {
        resizable: true,
        resizeObjectName: "PANEL",
        resizeAdornmentTemplate: nodeResizeAdornmentTemplate,
      },
      new go.Binding("angle").makeTwoWay(),
      $(
        go.Panel,
        "Auto",
        { name: "PANEL" },
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(
          go.Size.stringify
        ),
        $(
          go.Shape,
          "Rectangle",
          {
            portId: "",
            fromLinkable: true,
            toLinkable: true,
            minSize: new go.Size(75, 10),
            cursor: "pointer",
            // maxSize: new go.Size(80,40),
            fill: "white",
            strokeWidth: 1,
            stroke: "",
          },
          new go.Binding("figure"),
          new go.Binding("fill", "Background Color"),
          new go.Binding("stroke", "Border Color"),
        ),
        $(
          go.TextBlock,
          {
            font: getFontNodeText(),
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
          },
          new go.Binding("text", "Note").makeTwoWay(),
          
        )
      ),
      $(
        go.TextBlock,
        {
          font: getFont(),
          alignment: go.Spot.Center,
          textAlign: "center",
          margin: 2,
          stroke: "Black",
        },
        new go.Binding("text")
      ),
      {
        contextMenu: getContextMenu("Note"), // define a context menu for each node
      }
    )
  );

  diagram.nodeTemplateMap.add(
    "Initial Note",
    $(
      go.Node,
      "Table",
      { resizable: false },
      $(
        go.Panel,
        "Spot",

        $(
          go.TextBlock,
          {
            font: "14pt sans-serif",
            stroke: "grey",
            margin: 8,
            maxSize: new go.Size(160, NaN),
            wrap: go.TextBlock.WrapFit,
          },
          new go.Binding("text")
        )
      )
    )
  );

  function getContextMenu(type) {
    return $(
      "ContextMenu", // that has one button
      $("ContextMenuButton", $(go.TextBlock, "Case Type"), {
        click: changeToCaseType,
      }),

      $("ContextMenuButton", $(go.TextBlock, "Disposition OutCome"), {
        click: changeToDispositionOutcome,
      }),

      $("ContextMenuButton", $(go.TextBlock, "step"), { click: changeToStep }),

      $("ContextMenuButton", $(go.TextBlock, "Task"), { click: changeToTask }),

      $("ContextMenuButton", $(go.TextBlock, "Note"), { click: changeToNote }),

      $("ContextMenuButton", $(go.TextBlock, "OutCome"), {
        click: changeToOutcome,
      })
    );
  }

  //Change existing node to case type
  function changeToCaseType(e, obj) {
    diagram.commit(function (d) {
      var contextmenu = obj.part;
      var nodedata =[];
       nodedata = contextmenu.data;
      for (var i in data) {
        if (data[i].category === "Case Type") {
          diagram.model.removeNodeData(nodedata);
          diagram.model.addNodeData(data[i]);
          d.model.set(data[i], "loc", nodedata.loc);
          d.model.setDataProperty(data[i], "text", "");
          d.model.setDataProperty(data[i], "Case Type", "Case Type");
        }
      }
    }, "changed Case type");
  }

  //Change existing node to Disposition Outcome

  function changeToDispositionOutcome(e, obj) {
    diagram.commit(function (d) {
      var contextmenu = obj.part;
      var nodedata = contextmenu.data;
      for (var i in data) {
        if (data[i].category === "Disposition Outcome") {
          diagram.model.removeNodeData(nodedata);
          diagram.model.addNodeData(data[i]);
          d.model.set(data[i], "loc", nodedata.loc);
          d.model.setDataProperty(data[i], "text", "");
          d.model.setDataProperty(
            data[i],
            "Disposition Outcome",
            "Disposition Outcome"
          );
        }
      }
    }, "changed type");
  }

  //Change existing node to Task

  function changeToTask(e, obj) {
    diagram.commit(function (d) {
      var contextmenu = obj.part;
      var nodedata = contextmenu.data;
      for (var i in data) {
        if (data[i].category === "Task") {
          diagram.model.removeNodeData(nodedata);
          diagram.model.addNodeData(data[i]);
          d.model.set(data[i], "loc", nodedata.loc);
          d.model.setDataProperty(data[i], "text", "");
          d.model.setDataProperty(data[i], "Task", "Task");
        }
      }
    }, "changed type");
  }

  //Change existing node to Outcome

  function changeToOutcome(e, obj) {
    diagram.commit(function (d) {
      var contextmenu = obj.part;
      var nodedata = contextmenu.data;
      for (var i in data) {
        if (data[i].category === "Outcome") {
          diagram.model.removeNodeData(nodedata);
          diagram.model.addNodeData(data[i]);
          d.model.set(data[i], "loc", nodedata.loc);
          d.model.setDataProperty(data[i], "text", "");
          d.model.setDataProperty(data[i], "Outcome", "Outcome");
        }
      }
    }, "changed type");
  }

  //Change existing node to Step

  function changeToStep(e, obj) {
    diagram.commit(function (d) {
      var contextmenu = obj.part;
      var nodedata = contextmenu.data;
      for (var i in data) {
        if (data[i].category === "step") {
          diagram.model.removeNodeData(nodedata);
          diagram.model.addNodeData(data[i]);
          d.model.set(data[i], "loc", nodedata.loc);
          d.model.setDataProperty(data[i], "text", "");
          d.model.setDataProperty(data[i], "step", "Step");
        }
      }
    }, "changed type");
  }

  //Change existing node to Note
  function changeToNote(e, obj) {
    diagram.commit(function (d) {
      var contextmenu = obj.part;
      var nodedata = contextmenu.data;
      for (var i in data) {
        if (data[i].category === "Note") {
          diagram.model.removeNodeData(nodedata);
          diagram.model.addNodeData(data[i]);
          d.model.set(data[i], "loc", nodedata.loc);
          d.model.setDataProperty(data[i], "text", "");
          d.model.setDataProperty(data[i], "Note", "Note");
        }
      }
    }, "changed type");
  }
}
