import { toast } from "react-toastify";
import * as go from "gojs";
import {errorMessages,maxLength} from "../constants/Constants"

var diagramRef = "";
var isInvalidNode = false;
var isValidCasetype = false;
export function setReference(diagram) {
  diagramRef = diagram;
}

export function getReference(){
  return diagramRef;
}
export function validateWorkflow() {
    clearErrorMessage();
    const myDiagram = diagramRef;
    var diagramArray = JSON.parse(myDiagram.model.toJson()).nodeDataArray;
    var linkData = JSON.parse(myDiagram.model.toJson()).linkDataArray;
  // highlightLoops()  
if(!checkIfWorkflowNotEmpty(diagramArray)){
 if (checkIfCaseTypeExist()) {
    if (!validateNodeLinks(myDiagram,diagramArray,linkData)) {
      if (!validateCaseType(myDiagram,diagramArray)) {
        if (!validateNode(myDiagram,diagramArray)) {
          if(!validateLength(myDiagram,diagramArray)){
          if (!validateLinks(myDiagram,diagramArray,linkData)) {
            if(!  checkDuplicateOutcome(myDiagram,diagramArray,linkData)){
            return false;
          }
        }
        }
        }
      }
    }
   } else {
    toast.error(errorMessages.CASE_TYPE_NOT_FOUNT, {
      position: toast.POSITION.BOTTOM_RIGHT,
    });
  } 
}
 else {
    toast.error(errorMessages.EMPTY_WORKFLOW, {
      position: toast.POSITION.BOTTOM_RIGHT,
    });
}
  return true;
}


function checkIfWorkflowNotEmpty(diagramArray) {
  if (diagramArray && (Object.entries(diagramArray).length===0)) {
    return true;
  }
  return false;
}


function highlightNodes(data,linkData){
  const myDiagram = diagramRef;
  for (var d in data) {
    for(var i in data[d]){
      var cycle = data[d][i];
      for(var j in cycle){
       var from = cycle[j];
     //  var node = myDiagram.findNodeForKey(from)
  /*     var diagram = node.diagram;
      diagram.startTransaction("highlight");    
        node.isHighlighted= true
      diagram.commitTransaction("highlight"); */
        for(var k in cycle)
        {
          var to = cycle[k];
        if (linkData) {
          for (var h in linkData) {
            if (
              parseInt(linkData[h]["from"]) === parseInt(from) &&
              parseInt(linkData[h]["to"]) === parseInt(to)
            ) {
              myDiagram.findLinkForKey(linkData[h].key).isHighlighted = true;
            }
          }
        }
      }
    }
      /* var node = myDiagram.findNodeForKey(from)
      var diagram = node.diagram;
      diagram.startTransaction("highlight");
      node.isHighlighted= true
      diagram.commitTransaction("highlight"); */
  }
}
}

export function checkImmidieateLoops(highlight){
  const myDiagram = diagramRef;
  var linkData = JSON.parse(myDiagram.model.toJson()).linkDataArray;
  for(var i in linkData){
    var checkFor = parseInt(linkData[i]["from"]);
    var checkTo = parseInt(linkData[i]["to"]);
    for(var j in linkData){
      if(checkFor === parseInt(linkData[j]["to"]) && checkTo === parseInt(linkData[j]["from"])){
        console.log("loop detected",checkFor,"--->",checkTo)
        if(highlight){
          myDiagram.findLinkForKey(linkData[j].key).isHighlighted = true;
          myDiagram.findLinkForKey(linkData[i].key).isHighlighted = true;

        }
        return true
      }
    }
  }
return false
}
export function highlightLoops(data) {
  const myDiagram = diagramRef;
  var linkData = JSON.parse(myDiagram.model.toJson()).linkDataArray;
  checkImmidieateLoops(myDiagram,true);
  highlightNodes(data,linkData)
  var loopExist = false;
  if (data) {
  /*   loopExist = true;
    var data = loopData[0];
    for (var i in loopData) {
      data = loopData[i];
    }
    for (var j in data) {pro
      arr.push(parseInt(j));
    } */
    for (var d in data) {
      const myDiagram = diagramRef;
      //console.log("from",Object.keys(from),"to",data[d])
      for(var i in data[d]){
        var from = i;
      var to = data[d][i];
      
      var results = myDiagram.findNodeForKey(to);
     /*  if (results) {
        var node = results.findNodesOutOf();
        while (node.next()) {
          var child = node.value;

          if (arr.includes(child.data.key)) {
          }
        }
      } */
      myDiagram.commandHandler.scrollToPart(results);
      if (linkData) {
        for (var j in linkData) {
          if (
            parseInt(linkData[j]["from"]) === parseInt(from) &&
            parseInt(linkData[j]["to"]) === parseInt(to)
          ) {
            myDiagram.findLinkForKey(linkData[j].key).isHighlighted = true;
          } /* else if( parseInt(linkData[j]["from"]) === parseInt(to) &&
          parseInt(linkData[j]["to"]) === parseInt(from)) {
            myDiagram.findLinkForKey(linkData[j].key).isHighlighted = true;
          } */
        }
      }
    }}
  } else {
    loopExist = true;
  }
  return loopExist;
}

export function setLinkData() {
  const myDiagram = diagramRef;
  var linkData = JSON.parse(myDiagram.model.toJson()).linkDataArray;
  return JSON.stringify(linkData);
}

function compareLinkData(linkData, results, myDiagram) {
  var checkFromValid = false;
  var checkToValid = false;
  if (linkData) {
    for (var j in linkData) {
      if (results.data.key === linkData[j]["from"]) {
        checkFromValid = true;
      }
      if (results.data.key === linkData[j]["to"]) {
        checkToValid = true;
      }
    }
    
    if (results.data.category === "Case Type") {
      if (checkFromValid) {
        return true;
      } else {
        hightlightInvalidNode(myDiagram, results, errorMessages.BROKEN_LINK);
        return false;
      }
    } 
    
    else if (results.data.category === "Outcome") {
      if (checkToValid) {
        return true;
      } else {
        hightlightInvalidNode(
          myDiagram,
          results,
          errorMessages.INVALID_OUTCOME_NODE_POSITION
        );
        return false;
      }
    }
    
    else if(results.data.category === "Disposition Outcome" || results.data.category === "Task"){
      if (checkFromValid ) {
        return true;
      } else {
        hightlightInvalidNode(myDiagram, results, errorMessages.BROKEN_LINK);
        return false;
      }
      
    }
  } else {
    hightlightInvalidNode(myDiagram, results, errorMessages.BROKEN_LINK);
    return false;
  }
}
 
function checkIfCaseTypeExist() {
  const myDiagram = diagramRef;
  var diagramArray = JSON.parse(myDiagram.model.toJson()).nodeDataArray;
  for (var key in diagramArray) {
    if (diagramArray.hasOwnProperty(key)) {
      if (diagramArray[key].category === "Case Type") {
        return true;
      }
    }
  }
  return false;
} 

function validateLinks(myDiagram,diagramArray,linkData) {
  var validateLink = true;
  for (var key in diagramArray) {
    if (diagramArray.hasOwnProperty(key)) {
      var results = myDiagram.findNodeForKey(diagramArray[key].key);
      if (diagramArray[key].category === "Outcome") {
        
        if (!compareLinkData(linkData, results, myDiagram)) {
          validateLink = true;
          break;
        }
        else{
          validateLink = false
        }
      } else if (diagramArray[key].category === "Task") {
        if (!compareLinkData(linkData, results, myDiagram)) {
          validateLink = true;
          break;
        }
        else{
          validateLink = false
        }
      } else if (diagramArray[key].category === "Disposition Outcome") {
        if (!compareLinkData(linkData, results, myDiagram)) {
          validateLink = true;
          break;
        }
        else{
          validateLink = false
        }
      } else if (diagramArray[key].category === "Case Type") {
        if (!compareLinkData(linkData, results, myDiagram)) {
          validateLink = true;
          break;
        }
        else{
          validateLink = false
        }
      } else {
        validateLink = false;
         clearHighlight(myDiagram, results);
      }
    }
  }
  return validateLink;
}

function hightlightInvalidNode(myDiagram, results, message) {
  myDiagram.startTransaction("highlight search");
  myDiagram.highlight(results, "validate", "validate");
  myDiagram.commandHandler.scrollToPart(results);
  myDiagram.select(results);
  myDiagram.model.setDataProperty(results.data, "errorText", message);
  myDiagram.model.setDataProperty(results.data, "stroke", "red");
  myDiagram.model.setDataProperty(results.data, "font", "8pt Arial");
  myDiagram.commitTransaction("highlight search");
}

function clearHighlight(myDiagram, results) {
  if(results && results.data)
  myDiagram.model.setDataProperty(results.data, "errorText", "");
  myDiagram.clearHighlighteds();
}

export function clearErrorMessage() {
  const myDiagram = diagramRef;
  myDiagram.clearHighlighteds();
  var diagramArray = JSON.parse(myDiagram.model.toJson()).nodeDataArray;
  for (var key in diagramArray) {
    var results = myDiagram.findNodeForKey(diagramArray[key].key);
    if (results && results.data) {
      myDiagram.model.setDataProperty(results.data, "errorText", "");
    }
  }
}

export function linkProblemConverter(sel) {
  const myDiagram = diagramRef;
  var dash = [3, 2];
  myDiagram.selection.each(function (part) {
    if (part instanceof go.Link) {
      var node = myDiagram.findNodeForKey(part.data.to);
      var linksConnected = node.findLinksInto();
      var count = 0;
      while (linksConnected.next()) {
        count++;
        if (count > 1) {
          linksConnected.value.path.strokeDashArray = dash;
          linksConnected.value.path.stroke = "blue";

        }
      }
    }
  });

  /* if (msg) return "red";
  return "gray"; */
}


function validateNodeLinks(myDiagram,diagramArray,linkData) {
  var caseTypeLinks = false;
  for (var key in diagramArray) {
    if (diagramArray.hasOwnProperty(key)) {
    //  if (diagramArray[key].category === "Case Type") {
        //caseTypeNode =true
        if (linkData) {
          for (var j in linkData) {
            var fromLink = myDiagram.findNodeForKey(linkData[j]["from"]);
            var toLink = myDiagram.findNodeForKey(linkData[j]["to"]);

            if (fromLink.data.category === "Case Type" && toLink.data.category !== "Task") {
                  hightlightInvalidNode(myDiagram,toLink,errorMessages.INVALID_NODE_AFTER_CASE_TYPE);
                  caseTypeLinks = true;
                  break;
                  } 
             else {
                    caseTypeLinks = false;
                    clearHighlight(myDiagram, toLink);
                  }

            if (fromLink.data.category ==="Disposition Outcome" && toLink.data.category !== "Task") {
                    hightlightInvalidNode(
                      myDiagram,
                      toLink,
                      errorMessages.INVALID_NODE_AFTER_DISPOSITION_OUTCOME
                    );
                    caseTypeLinks = true;
                    break;
                  } else {
                    caseTypeLinks = false;
                    clearHighlight(myDiagram, fromLink);
                  }

                  if (fromLink.data.category ==="Task" && toLink.data.category !== "Outcome") {
                    hightlightInvalidNode(
                      myDiagram,
                      toLink,
                      errorMessages.INVALID_NODE_AFTER_TASK
                    );
                    caseTypeLinks = true;
                    break;
                  } else {
                    caseTypeLinks = false;
                    clearHighlight(myDiagram, fromLink);
                  }

                
                      /* if (fromLink.data.category !== "step" &&  toLink.data.category === "Case Type"
            ) {
              caseTypeLinks = true;
              hightlightInvalidNode(
                myDiagram,
                toLink,
                  errorMessages.INVALID_CASE_TYPE_NODE_POSITION
              );
             // break;
            } else {
              caseTypeLinks = false;
              clearHighlight(myDiagram, toLink);
            } */
          }
        }
     // }
    }
   return caseTypeLinks;
  }
  /*  if(!caseTypeNode){
      caseTypeLinks = false;
      toast.error(
        "No Case Type node found in workflow",
        {
          position: toast.POSITION.BOTTOM_RIGHT,
        }
      );
  } */

  return caseTypeLinks;
}

function validateCaseType(myDiagram,diagramArray) {
  var count = 0;
  //var nodesBeforeCaseType;
  for (var key in diagramArray) {
    var results = myDiagram.findNodeForKey(diagramArray[key].key);
    if (diagramArray.hasOwnProperty(key)) {
      if (diagramArray[key].category === "Case Type") {
        count++;
      }
    }
    if (count > 1) {
      isValidCasetype = true;
      hightlightInvalidNode(
        myDiagram,
        results,
        errorMessages.INVALID_NUMBER_OF_CASE_TYPE
      );
      break;
    } else {
      isValidCasetype = false;
    }
  }
  return isValidCasetype;
}


//Validate Properties Associated With Each Node
export function validateNode(myDiagram,diagramArray) {
  myDiagram.clearHighlighteds();
  for (var key in diagramArray) {
    if (diagramArray.hasOwnProperty(key)) {
      var results = myDiagram.findNodeForKey(diagramArray[key].key);
      if (diagramArray[key].category === "Outcome") {
        if (!results.data.Outcome || !results.data["Activity Type"]) {
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_NODE_MESSAGE);
          isInvalidNode = true;
          break;
        }
        else{
          isInvalidNode = false;
          clearHighlight(myDiagram, results);
        }
      } else if (diagramArray[key].category === "Case Type") {
        if (
          !results.data["Case Category"]
        ) {
          isInvalidNode = true;
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_NODE_MESSAGE);
          break;
        } else {
          isInvalidNode = false;
          clearHighlight(myDiagram, results);
        }
      } else if (diagramArray[key].category === "Task") {
        if (
          !results.data["Task"] ||
          !results.data["Activity"] ||
          !results.data["Standard Due Date"] ||
          !results.data["Expedited Due Date"]
        ) {
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_NODE_MESSAGE);
          isInvalidNode = true;
          break;
        } else {
          clearHighlight(myDiagram, results);
          isInvalidNode = false;
        }
      } else if (diagramArray[key].category === "Disposition Outcome") {
        if (
          !results.data["Review Type"] ||
          !results.data["Disposition Outcome"]
        ) {
          hightlightInvalidNode(myDiagram, results,errorMessages.INVALID_NODE_MESSAGE);
          isInvalidNode = true;
          break;
        } else {
          clearHighlight(myDiagram, results);
        }
      }
    }
  }
  return isInvalidNode;
}

function checkDuplicateOutcome(myDiagram,diagramArray,linkData) {
  var duplicate =  false;
  var map ={};
  var arr=[];
        if (linkData) {
          for (var j in linkData) {
            var fromLink = myDiagram.findNodeForKey(linkData[j]["from"]);
            var toLink = myDiagram.findNodeForKey(linkData[j]["to"]);
  if (fromLink.data.category ==="Task" && toLink.data.category === "Outcome") {
    arr.push({
     "Outcome" :toLink.data["Outcome"],
     "Task":    fromLink.data["key"]
    })
   }
  }
  console.log(arr)

  for(let i = 0; i < arr.length; i++) {
var task = arr[i].Task;
var count =0;
for(let j=0;j<arr.length;j++){
  if(task === arr[j].Task && arr[i].Outcome === arr[j].Outcome){
    console.log(task,"--",arr[j].Outcome)
    count++;
    if(count>1){
      duplicate = true;
      break
    }
  }
}

  /*   if(map[arr[i]]) {
       duplicate = true;
       break;
    }
    map[arr[i]] = true;
 } */
}
}
if(duplicate){
  hightlightInvalidNode(
    myDiagram,
    fromLink,
    errorMessages.DUPLICATE_OUTCOME_NODE
  );
 }
//}
 // }
 return duplicate;
}

function validateLength(myDiagram,diagramArray){
  var isInvalidLength = false;
  myDiagram.clearHighlighteds();
  for (var key in diagramArray) {
    if (diagramArray.hasOwnProperty(key)) {
      var results = myDiagram.findNodeForKey(diagramArray[key].key);
      if(results && results.data){
        if(results.data.Outcome && (results.data.Outcome.length >maxLength.OUTCOME_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.OUTCOME_MAX_LENGTH+" for field Outcome");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data.Task && (results.data.Task.length >maxLength.TASK_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.OUTCOME_MAX_LENGTH+" for field Task");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data.Activity && (results.data.Activity.length >maxLength.ACTIVITY_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.ACTIVITY_MAX_LENGTH+" for field Activity");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data.OActivity && (results.data.OActivity.length >maxLength.ACTIVITY_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.ACTIVITY_MAX_LENGTH+" for field Activity");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data["Case Type"] && (results.data["Case Type"].length >maxLength.CASE_TYPE_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.CASE_TYPE_MAX_LENGTH+" for field Case Type");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data["Activity Type"] && (results.data["Activity Type"].length >maxLength.CASE_TYPE_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.ACTIVITY_TYPE_MAX_LENGTH+" for field Activity Type");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data["Disposition Outcome"] && (results.data["Disposition Outcome"].length >maxLength.DISPOSITION_OUTCOME_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.DISPOSITION_OUTCOME_LENGTH+" for field Disposition Outcome");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }

        if(results.data["Review Type"] && (results.data["Review Type"].length >maxLength.REMARK_MAX_LENGTH)){
          hightlightInvalidNode(myDiagram, results, errorMessages.INVALID_LENGTH_MESSAGE+maxLength.REMARK_MAX_LENGTH+" for field Disposition Review Type");
          isInvalidLength = true;
          break;
        }
        else{
          isInvalidLength = false;
          clearHighlight(myDiagram, results);
        }
      }
      
    }
  }
  return isInvalidLength;
}